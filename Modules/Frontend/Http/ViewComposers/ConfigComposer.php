<?php
 namespace Modules\Frontend\Http\ViewComposers;

 use Illuminate\View\View;
 use Modules\Config\Repositories\ConfigRepository;
 use Modules\Temp\Repositories\TempRepository;
 use Modules\Temp\Entities\Temp;

 class ConfigComposer
 {
    private $config;
     /**
      * Create a movie composer.
      *
      * @return void
      */
     public function __construct(ConfigRepository $config)
     {
         $this->config = $config;
     }

     /**
      * Bind data to the view.
      *
      * @param  View  $view
      * @return void
      */
     public function compose(View $view)
     {
        $view->with([
          'config' => $this->config->all(),
          'temps' => Temp::where('slug','<>', 'gioi-thieu')->get(),
        ]);
     }
 }