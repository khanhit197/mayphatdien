<?php
 namespace Modules\Frontend\Http\ViewComposers;

 use Illuminate\View\View;
 use Modules\Blog\Repositories\CategoryblogRepository;
 use Modules\Trademark\Repositories\TrademarkRepository;
 use Modules\Category\Repositories\CategoryRepository;

 class HeaderComposer
 {
    private $categoriesBlogs;
    private $categories;
    private $tradeMark;
     /**
      * Create a movie composer.
      *
      * @return void
      */
     public function __construct(CategoryblogRepository $categoriesBlogs,TrademarkRepository $tradeMark,CategoryRepository $categories)
     {
        $this->categoriesBlogs = $categoriesBlogs;
        $this->tradeMark = $tradeMark;
        $this->categories = $categories;
     }

     /**
      * Bind data to the view.
      *
      * @param  View  $view
      * @return void
      */
     public function compose(View $view)
     {
        $view->with([
          'categoriesBlogs' => $this->categoriesBlogs->all(),
          'trademarks' => $this->tradeMark->getData(),
          'categories' => $this->categories->getCategoriesHeader(),
        ]);
     }
 }