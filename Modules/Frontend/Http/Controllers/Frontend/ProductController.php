<?php

namespace Modules\Frontend\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Product\Repositories\ProductRepository;
use Modules\Product\Entities\Product;
use Modules\Category\Repositories\CategoryRepository;
use Modules\Trademark\Repositories\TrademarkRepository;


class ProductController extends AdminBaseController
{
    private $product;
    private $category;
    private $trademark;

    public function __construct(ProductRepository $product, CategoryRepository $category, TrademarkRepository $trademark)
    {
        $this->product = $product;
        $this->category = $category;
        $this->trademark = $trademark;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function productSale(Request $request){
        $productSales = $this->product->productSales($request);
        if(count($productSales)){
            $trademarks = $this->trademark->all();
            $categories = $this->category->all();
            return view('frontend::frontend.pages.products.product-sale',compact('productSales','request','trademarks','categories'));
        }else{
            return view('frontend::frontend.partials.empty-product',['title' => 'Sale']);
        }
    }

    public function productCategory(Request $request){
        $category = $this->category->findBySlug($request->slug);
        if($category){
            $productCategories = $this->product->productCategory($category, $request);
            if(count($productCategories)){
                $trademarks = $this->trademark->all();
                return view('frontend::frontend.pages.products.product-category',compact('productCategories','request','category','trademarks'));
            }else{
                return view('frontend::frontend.partials.empty-product',['title' => $category->name]);
            }
        }
        abort(404);
        
    }

    public function store(Request $request){
        $stores = $this->product->stores($request);
        if(count($stores)){
            $trademarks = $this->trademark->all();
            return view('frontend::frontend.pages.products.store',compact('stores','request','trademarks'));
        }else{
            return view('frontend::frontend.partials.empty-product',['title' => 'Cửa hàng']);
        }
    }

    public function productSubCategory(Request $request){
        $category = $this->category->findBySlug($request->subcategory);

        if($category){
            $productCategories = $this->product->filter($category->product(), $request);
            $productCategories = $productCategories->paginate(20);
            if(count($productCategories)){
                $trademarks = $this->trademark->all();
                return view('frontend::frontend.pages.products.product-category',compact('productCategories','request','category','trademarks'));
            }else{
                return view('frontend::frontend.partials.empty-product',['title' => $category->name]);
            }
        }
        abort(404);
    }


    public function detailProduct($slug){
        $product = $this->product->findBySlug($slug);
        if($product){
            $gallerys = explode(',', $product->gallery);
            $parentCategory = $this->category->find($product->parent_category);
            return view('frontend::frontend.pages.products.detail-product',compact('product','gallerys','parentCategory'));
        }
        abort(404);
    }
}
