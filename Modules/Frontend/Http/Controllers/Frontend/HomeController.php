<?php

namespace Modules\Frontend\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
// use Cart;
// use Modules\Store\Repositories\StoreRepository;
// use Modules\Temp\Repositories\TempRepository;
// use Modules\Product\Repositories\ProductRepository;
// use Modules\Product\Entities\Product;
// use Modules\Banner\Repositories\BannerRepository;

class HomeController extends AdminBaseController
{
    /**
     * @var StoreRepository
     */
    private $store;
    /**
     * @var TempRepository
     */
    private $temp;
    /**
     * @var ProductRepository
     */
    private $product;
    /**
     * @var BannerRepository
     */
    private $banner;

    public function __construct()
    {
        parent::__construct();

        // $this->store = $store;
        // $this->temp = $temp;
        // $this->product = $product;
        // $this->banner = $banner;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    // Trang chủ website
    public function index(Request $request)
    {
        return view('frontend::pages.home');
    }

    // public function aboutUs()
    // {
    //     $stores = $this->store->all();
    //     $temp = $this->temp->findBySlug('gioi-thieu');
    //     return view('frontend::frontend.pages.about-us',compact('stores','temp'));
    // }

    // public function getPage($slug){
    //     $temp = $this->temp->findBySlug($slug);
    //     if($temp){
    //         return view('frontend::frontend.pages.detail-static',compact('temp'));
    //     }
    // }

    // public function search(Request $request){
    //     if($request->keyword){
    //         $products = $this->product->pageSearch($request);
    //         return view('frontend::frontend.pages.search',compact('request','products'));
    //     }
    //     abort(404);
    // }

}
