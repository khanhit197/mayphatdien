<?php

namespace Modules\Frontend\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Product\Repositories\ProductRepository;
use Modules\Voucher\Entities\Voucher;
use Modules\City\Entities\City;
use Modules\City\Entities\Districts;
use Modules\Config\Entities\Config;
use Modules\Order\Entities\Order;
use Modules\Order\Entities\OrderDetail;
use Modules\Product\Entities\Product;
use Cart;
use Carbon\Carbon;

class CartController extends AdminBaseController
{
    /**
     * @var ProductRepository
     */
    private $product;

    public function __construct(ProductRepository $product)
    {
        parent::__construct();

        $this->product = $product;
    }
    
    public function addCart(Request $request){
        $product = $this->product->find($request->id);
        $price = $product->price;
        if($product->discount){
            $discount = 100 - $product->discount;
            $price = ($product->price * $discount) / 100;
        }
        Cart::add($product->id, $product->name, $request->quantity, $price);
        
        $view = view('frontend::frontend.pages.cart.list-product-to-cart', compact(''));
        return [
            'data'  =>  $view->render(),
            'total' =>  Cart::subtotal(0) . 'VND',
            'count' =>  Cart::count() . 'mục',
        ];
    }

    public function deleteCart(Request $request){
        Cart::remove($request->rowId);
        
        $view = view('frontend::frontend.pages.cart.list-product-to-cart', compact(''));
        return [
            'data'  =>  $view->render(),
            'total' =>  Cart::subtotal(0) . 'VND',
            'count' =>  Cart::count() . 'mục',
        ];
    }

    public function cartDetail(){
        return view('frontend::frontend.pages.cart.cart-detail', compact(''));
    }

    public function addVoucher(Request $request){
        $voucher = Voucher::where('code',$request->code)->first();
        if(isset($voucher) && !empty($voucher)){
            $today = Carbon::today();
            if(strtotime($voucher->start_date) <= strtotime($today) && strtotime($today) <= strtotime($voucher->end_date)){
                $arr = [];
                if(count(Cart::content())){
                    foreach (Cart::content() as $key => $value) {
                        $arr[] = $value->id;
                    }
                }
                
                $subtotal = Cart::subtotal(0,'','');
                if($voucher->type == 1){
                    $discount = $voucher->discount;
                    $total = $subtotal - $discount;

                }else{
                    $discount = ($subtotal * $voucher->discount) / 100;
                    $total = $subtotal - $discount;
                }
                 return [
                    'status'    =>  true,
                    'discount'    =>  formatPrice($discount).'VND',
                    'total'   =>  formatPrice($total).'VND',
                    'code'   =>  $request->code,
                ];
            }
        }
        return [
                'status'    =>  false,
                'message'   =>  'Mã khuyến mãi đã hết hạn sử dụng.
(hoặc) Mã khuyến mãi không hợp lệ. Vui lòng kiểm tra lại.',
            ];
    }

    public function cartAddress(){
        $cities = City::all();
        return view('frontend::frontend.pages.cart.cart-address', compact('cities'));
        
    }

    public function updateToCartPageCart(Request $request){
        $arrayRowID = explode(',', $request->arrRowId);
        $arrQuantity = explode(',', $request->arrQuantity);
        if(count($arrayRowID)){
            foreach ($arrayRowID as $key => $rowId) {
                Cart::update($rowId, $arrQuantity[$key]); 
            }
        }
        $view = view('frontend::frontend.pages.cart.list-product-to-cart-detail', compact(''));
        return [
            'data'  =>  $view->render(),
            'total' =>  Cart::subtotal(0) . 'VND',
        ];
    }

    public function getDistrict(Request $request){
        $items = Districts::where('city_id',$request->id)->get();
        $view = view('frontend::frontend.pages.cart.get-district',compact('items'));
        return $view->render();
    }

    public function feeShip(Request $request){
        $config = Config::first();
        $feeShip = 0;
        $item = Districts::find($request->id);
        $city = $item->city;
        if($city->fee_ship){
            $feeShip = $city->fee_ship;
        }else{
            if($item->fee_ship){
                $feeShip = $item->fee_ship;
            }
        }
        if($item->is_urban == 2){
            if($config->fee_ship_different){
                if(Cart::subtotal(0,'','') >= $config->fee_ship_different){
                    $feeShip = 0;
                }
            }
            
        }
        if($item->is_urban == 1){
            if($config->fee_ship_urban){
                if(Cart::subtotal(0,'','') >= $config->fee_ship_urban){
                    $feeShip = 0;
                }
            }
        }
        if($item->is_urban == 0){
            if($config->fee_ship_suburban){
                if(Cart::subtotal(0,'','') >= $config->fee_ship_suburban){
                    $feeShip = 0;
                }
            }
        }
        $total = Cart::subtotal(0,'','');
        $totalDiscount = 0;
        if($request->voucher_code){
            $voucher = Voucher::where('code',$request->voucher_code)->first();
            if(isset($voucher) && !empty($voucher)){
                if($voucher->type == 1){
                    $discount = $voucher->discount;
                }else{
                    $discount = ($total * $voucher->discount) / 100;
                } 
                $totalDiscount = $total - $discount;
            }
        }
        $total += $totalDiscount + $feeShip;
        return [
            'feeShip'  => formatPrice($feeShip).'đ',
            'feeShipNoFormat'  => $feeShip,
            'total'  => formatPrice($total).'đ',
            'totalDiscount'  => formatPrice($totalDiscount).'đ',
        ];
    }

    public function submitOrder(Request $request){
        $allRequest = $request->all();
        $feeShip = 0;
        if($request->fee_ship){
            $feeShip = $request->fee_ship;
        }
        $allRequest['total_price'] =  Cart::subtotal(0,'','');
        $allRequest['total_after_discount'] = $allRequest['total_price'] + $feeShip;
        if($request->voucher_code){
            $voucher = Voucher::where('code',$request->voucher_code)->first();
            if(isset($voucher) && !empty($voucher)){
                if($voucher->type == 1){
                    $allRequest['total_discount'] = $voucher->discount;
                }else{
                    $allRequest['total_discount'] = ($allRequest['total_price'] * $voucher->discount) / 100;
                } 
                $allRequest['total_after_discount'] = $allRequest['total_price'] - $allRequest['total_discount'];
            }
        }
        $order = Order::create($allRequest);
        if(count(Cart::content())){
            foreach (Cart::content() as $key => $value) {
                $product = Product::find($value->id);
                $data = [
                    'quantity'  => $value->qty,
                    'total_price'  => $value->qty * $value->price,
                    'product_id'  => $value->id,
                    'order_id'  => $order->id,
                    'price'  => $product->price,
                    'price_promo'  => $value->price,
                ];
                OrderDetail::create($data);
            }
        }
        Cart::destroy();
        return redirect()->route('frontend.home.home.home-page')->with('orderSuccess','1');
    }
}
