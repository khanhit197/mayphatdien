<?php

namespace Modules\Frontend\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Blog\Repositories\BlogRepository;
use Modules\Blog\Repositories\CategoryblogRepository;
use Modules\Blog\Entities\Blog;
use Modules\Blog\Entities\CategoryBlog;
use Modules\Trademark\Repositories\TrademarkRepository;
use Modules\Category\Repositories\CategoryRepository;
use Modules\Product\Repositories\ProductRepository;

class TradeMarkController extends AdminBaseController
{
    /**
     * @var TrademarkRepository
     */
    private $trademark;
    /**
     * @var CategoryRepository
     */
    private $category;
    /**
     * @var ProductRepository
     */
    private $product;

    public function __construct(TrademarkRepository $trademark,CategoryRepository $category,ProductRepository $product)
    {
        $this->trademark = $trademark;
        $this->category = $category;
        $this->product = $product;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function index()
    {
        $trademarks = $this->trademark->getData();
        return view('frontend::frontend.pages.trademarks.index',compact('trademarks'));
    }

    public function getProducts(Request $request)
    {
        $trademark = $this->trademark->findBySlug($request->slug);
        if($trademark){
            $products = $this->product->productTrademark($trademark, $request);
            $categories = $this->category->all();
            return view('frontend::frontend.pages.trademarks.list-product',compact('trademark', 'products','request','categories'));
        }
        abort(404);
    }
}
