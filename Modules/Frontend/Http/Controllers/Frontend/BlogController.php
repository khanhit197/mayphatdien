<?php

namespace Modules\Frontend\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Blog\Repositories\BlogRepository;
use Modules\Blog\Repositories\CategoryblogRepository;
use Modules\Blog\Entities\Blog;
use Modules\Blog\Entities\CategoryBlog;

class BlogController extends AdminBaseController
{
    /**
     * @var BlogRepository
     */
    private $blog;
    /**
     * @var CategoryblogRepository
     */
    private $categoryBlog;

    public function __construct(BlogRepository $blog,CategoryblogRepository $categoryBlog)
    {
        $this->blog = $blog;
        $this->categoryBlog = $categoryBlog;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function listBlog()
    {
        $blogs = $this->blog->paginate();
        $category = '';
        return view('frontend::frontend.pages.blogs.blog',compact('blogs','category'));
    }

    public function getBlog($slug)
    {
        $blog = $this->blog->findbySlug($slug);
        $categoriesBlogs = $blog->categoryBlog;
        $previous = Blog::where('id', '<', $blog->id)->first();
        $next = Blog::where('id', '>', $blog->id)->first();
        return view('frontend::frontend.pages.blogs.detail-blog',compact('blog','categoriesBlogs', 'previous', 'next'));
    }

    public function getCategoriesBlog($slug){
        $category=  $this->categoryBlog->findbySlug($slug);
        $blogs = $category->blogs()->paginate(5);
        return view('frontend::frontend.pages.blogs.blog',compact('blogs','category'));
    }
}
