<?php

use Illuminate\Routing\Router;
/** @var Router $router */
$router->group(['prefix' =>'/thuong-hieu'], function (Router $router) {
	$router->get('/', [
	    'uses' => 'TradeMarkController@index',
	    'as' => 'frontend.trademark.trademark.index',
		'middleware' => config('asgard.page.config.middleware'),
	]);

	$router->get('/{slug}', [
	    'uses' => 'TradeMarkController@getProducts',
	    'as' => 'frontend.trademark.trademark.get-products',
		'middleware' => config('asgard.page.config.middleware'),
	]);
});