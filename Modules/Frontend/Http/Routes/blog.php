<?php

use Illuminate\Routing\Router;
/** @var Router $router */
$router->group(['prefix' =>'/blog'], function (Router $router) {
	$router->get('/danh-sach', [
	    'uses' => 'BlogController@listBlog',
	    'as' => 'frontend.blog.blog.list-blogs',
		'middleware' => config('asgard.page.config.middleware'),
	]);

	$router->get('/{slug}', [
	    'uses' => 'BlogController@getBlog',
	    'as' => 'frontend.blog.blog.detail-blog',
		'middleware' => config('asgard.page.config.middleware'),
	]);
});

$router->get('/danh-muc-blog/{slug}', [
    'uses' => 'BlogController@getCategoriesBlog',
    'as' => 'frontend.blog.blog.get-categories-blog',
	'middleware' => config('asgard.page.config.middleware'),
]);