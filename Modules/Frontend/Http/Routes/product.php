<?php

use Illuminate\Routing\Router;
/** @var Router $router */
$router->group(['prefix' =>'/san-pham'], function (Router $router) {
	$router->get('/san-pham-sale', [
	    'uses' => 'ProductController@productSale',
	    'as' => 'frontend.product.product.list-product-sale',
		'middleware' => config('asgard.page.config.middleware'),
	]);
	$router->get('/{slug}', [
	    'uses' => 'ProductController@detailProduct',
	    'as' => 'frontend.product.product.detail-product',
		'middleware' => config('asgard.page.config.middleware'),
	]);
});
$router->get('/cua-hang', [
    'uses' => 'ProductController@store',
    'as' => 'frontend.product.product.store',
	'middleware' => config('asgard.page.config.middleware'),
]);
$router->get('/danh-muc/{slug}', [
    'uses' => 'ProductController@productCategory',
    'as' => 'frontend.product.product.product-category',
	'middleware' => config('asgard.page.config.middleware'),
]);
$router->get('/danh-muc/{slug}/{subcategory}', [
    'uses' => 'ProductController@productSubCategory',
    'as' => 'frontend.product.product.product-sub-category',
	'middleware' => config('asgard.page.config.middleware'),
]);