<?php

use Illuminate\Routing\Router;
/** @var Router $router */
$router->group(['prefix' =>'/gio-hang'], function (Router $router) {
	$router->post('/add-cart', [
	    'uses' => 'CartController@addCart',
	    'as' => 'frontend.cart.cart.add-cart',
		'middleware' => config('asgard.page.config.middleware'),
	]);
	$router->post('/delete-cart', [
	    'uses' => 'CartController@deleteCart',
	    'as' => 'frontend.cart.cart.delete-cart',
		'middleware' => config('asgard.page.config.middleware'),
	]);
	$router->post('/add-voucher', [
	    'uses' => 'CartController@addVoucher',
	    'as' => 'frontend.cart.cart.add-voucher',
		'middleware' => config('asgard.page.config.middleware'),
	]);
	$router->post('/update-to-cart-page-cart', [
	    'uses' => 'CartController@updateToCartPageCart',
	    'as' => 'frontend.cart.cart.updateToCartPageCart',
		'middleware' => config('asgard.page.config.middleware'),
	]);
	$router->get('/chi-tiet-don-hang', [
	    'uses' => 'CartController@cartDetail',
	    'as' => 'frontend.cart.cart.cart-detail',
		'middleware' => config('asgard.page.config.middleware'),
	]);
	$router->get('/xac-nhan-don-hang', [
	    'uses' => 'CartController@cartAddress',
	    'as' => 'frontend.cart.cart.cart-address',
	    'middleware' => config('asgard.page.config.middleware'),
	]);
	$router->post('/get-district', [
	    'uses' => 'CartController@getDistrict',
	    'as' => 'frontend.cart.cart.get-district',
	    'middleware' => config('asgard.page.config.middleware'),
	]);
	$router->post('/fee-ship', [
	    'uses' => 'CartController@feeShip',
	    'as' => 'frontend.cart.cart.fee-ship',
	    'middleware' => config('asgard.page.config.middleware'),
	]);
	$router->post('cart/submit-order', [
	    'uses' => 'CartController@submitOrder',
	    'as' => 'frontend.cart.cart.submit-order'
	]);
});