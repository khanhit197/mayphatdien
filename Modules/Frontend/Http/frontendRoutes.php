<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->get('/', [
    'uses' => 'HomeController@index',
    'as' => 'homepage',
    // 'middleware' => config('asgard.page.config.middleware'),
]);



// $router->get('/', [
//     'uses' => 'HomeController@index',
//     'as' => 'frontend.home.home.home-page',
// 	'middleware' => config('asgard.page.config.middleware'),
// ]);
// $router->get('/tim-kiem', [
//     'uses' => 'HomeController@search',
//     'as' => 'frontend.home.home.search',
// 	'middleware' => config('asgard.page.config.middleware'),
// ]);
// $router->get('/gioi-thieu', [
//     'uses' => 'HomeController@aboutUs',
//     'as' => 'frontend.home.home.about-us',
// 	'middleware' => config('asgard.page.config.middleware'),
// ]);
// $router->get('/trang/{slug}', [
//     'uses' => 'HomeController@getPage',
//     'as' => 'frontend.home.home.get-page',
// 	'middleware' => config('asgard.page.config.middleware'),
// ]);


// include_once('Routes/blog.php');
// include_once('Routes/trademark.php');
// include_once('Routes/product.php');
// include_once('Routes/cart.php');