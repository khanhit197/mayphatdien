<div class="store__secondary">
    <div class="store__area"><span class="ttl">Brands</span><div class="c-select1"><select class="c-select1__list">
                <option selected="selected" disabled="disabled">Chọn Brand</option>
                <option value="ao-khoac">Áo Khoác</option>
                <option value="ao-khoac">Áo Khoác</option>
                <option value="ao-khoac">Áo Khoác</option>
                <option value="ao-khoac">Áo Khoác</option>
            </select>
            <div class="c-select1__arrow"><i></i></div>
        </div>
    </div>
    <div class="store__area"><span class="ttl">Lọc theo giá</span>
        <form action="" method="get">
            <div class="price-slide" id="slider-range" data-price-min="150000" data-price-max="29900000"></div>
            <div class="price-slide-amount"><input type="number" id="price-filter-min" /><input type="number" id="price-filter-max" /><button class="button" type="submit">Lọc</button>
                <div class="price-label">Giá <label class="from" id="form">150,000</label> — <span class="to" id="to">29,900,000</span></div>
            </div>
        </form>
    </div>
</div>