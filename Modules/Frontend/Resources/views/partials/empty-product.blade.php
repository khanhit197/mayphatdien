@extends('frontend::frontend.layouts.master')
@section('content')
	
	<div class="breadcrumb">
        <div class="breadcrumb__container layout__container">
            <nav><a href="{{ route('frontend.home.home.home-page') }}">Trang chủ</a></nav>
        </div>
    </div>
	<div class="layout__container layout__spacing-menu">
        <header class="title__header">
            <h1>{{ $title }}</h1>
        </header>
        <div class="noti__info checkout__coupon-action"><span>Không tìm thấy sản phẩm nào khớp với lựa chọn của bạn. </span></div>
    </div>
@endsection
@section('js')
@endsection