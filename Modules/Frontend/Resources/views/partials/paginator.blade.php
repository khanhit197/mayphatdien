@if ($paginator->hasPages())
    <!-- Pagination -->
    <nav class="c-pagination2">
        <ul class="c-pagination2__list">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li style="display:none"><a class="c-pagination2__number prev" href="#">←</a></li>
            @else
                <li><a class="c-pagination2__number prev" href="{{ $paginator->previousPageUrl() }}">←</a></li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        <li><a href="{{ $url }}"><span class="c-pagination2__number {{ ($page == $paginator->currentPage()) ? 'current' : ''}}">{{ $page }}</span></a></li>
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li><a class="c-pagination2__number next" href="{{ $paginator->nextPageUrl() }}">→</a></li>
            @else
                <li style="display:none"><a class="c-pagination2__number next" href="{{ $paginator->nextPageUrl() }}">→</a></li>
            @endif
        </ul>
    </nav>
    <!-- Pagination -->
@endif