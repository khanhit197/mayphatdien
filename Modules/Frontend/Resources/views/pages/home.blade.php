@extends('frontend::layouts.master')


@section('content')
    
    <!-- Hero Section Begin -->
    <section class="section">
<!--         <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="hero-text">
                        <h1>Các dòng máy phát điện</h1>
                        <p>Luôn đi đầu trong các dòng máy phát điện có công nhỏ nhiều ưu điểm </p>
                        <a href="#" class="primary-btn">Tìm hiểu ngay</a>
                    </div>
                </div>
                
            </div>
        </div> -->
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="/assets/media/images/banner/banner-1.jpg" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="/assets/media/images/banner/banner-2.jpg" alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="/assets/media/images/banner/banner-3.jpg" alt="Third slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </section>
    <!-- Hero Section End -->

    <!-- About Us Section Begin -->
    <section class="aboutus-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="about-text">
                        <div class="section-title">
                            <span>Về chúng tôi</span>
                            <h2>Công ty ABC</h2>
                        </div>
                        <p class="f-para">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi possimus unde similique quod error sapiente incidunt. Ad est ut fugiat neque deleniti fuga voluptas nemo sit voluptates tempore saepe eius aperiam inventore, harum cumque earum nesciunt placeat reprehenderit nam illum, rerum, delectus ducimus velit vel. Amet quibusdam accusantium, at!</p>

                        <a href="#" class="primary-btn about-btn">Tìm hiểu thêm</a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="about-pic">
                        <div class="row">
                            <div class="col-sm-6">
                                <img src="/assets/media/images/tmp/sua-may-phat-dien-2018-3_mediumThumb.jpg" alt="">
                            </div>
                            <div class="col-sm-6">
                                <img src="/assets/media/images/tmp/ezgif-7-dc8dff0fecfd_mediumThumb.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- About Us Section End -->

    <!-- Services Section End -->
    <section class="services-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>Dịch vụ của chúng tôi</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    <div class="service-item">
                        <img src="/assets/media/3-untitled-8.jpg" alt="">
                        
                        <h4>Cho thuê máy phát điện</h4>
                        <div class="row">
                            <div class="col-2">
                                <i class="flaticon-036-parking"></i>
                            </div>
                            <div class="col-10">
                                <p class="text-left">Công ty Hưng thịnh chuyên mua bán , sửa chữa, bảo trì, cho thuê tất cả các loại máy phát điện công nghiệp từ 15kva - 2500kva. Nhanh chóng giá rẻ tận nơi. có mặt nhanh chóng hỗ trợ khách hàng.</p>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="service-item">
                        <img src="/assets/media/3-untitled-8.jpg" alt="">
                        
                        <h4>Cho thuê máy phát điện</h4>
                        <div class="row">
                            <div class="col-2">
                                <i class="flaticon-036-parking"></i>
                            </div>
                            <div class="col-10">
                                <p class="text-left">Công ty Hưng thịnh chuyên mua bán , sửa chữa, bảo trì, cho thuê tất cả các loại máy phát điện công nghiệp từ 15kva - 2500kva. Nhanh chóng giá rẻ tận nơi. có mặt nhanh chóng hỗ trợ khách hàng.</p>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="service-item">
                        <img src="/assets/media/3-untitled-8.jpg" alt="">
                        
                        <h4>Cho thuê máy phát điện</h4>
                        <div class="row">
                            <div class="col-2">
                                <i class="flaticon-036-parking"></i>
                            </div>
                            <div class="col-10">
                                <p class="text-left">Công ty Hưng thịnh chuyên mua bán , sửa chữa, bảo trì, cho thuê tất cả các loại máy phát điện công nghiệp từ 15kva - 2500kva. Nhanh chóng giá rẻ tận nơi. có mặt nhanh chóng hỗ trợ khách hàng.</p>
                            </div>
                        </div>
                        
                    </div>
                </div>

                <div class="col-lg-4 col-sm-6">
                    <div class="service-item">
                        <img src="/assets/media/3-untitled-8.jpg" alt="">
                        
                        <h4>Cho thuê máy phát điện</h4>
                        <div class="row">
                            <div class="col-2">
                                <i class="flaticon-036-parking"></i>
                            </div>
                            <div class="col-10">
                                <p class="text-left">Công ty Hưng thịnh chuyên mua bán , sửa chữa, bảo trì, cho thuê tất cả các loại máy phát điện công nghiệp từ 15kva - 2500kva. Nhanh chóng giá rẻ tận nơi. có mặt nhanh chóng hỗ trợ khách hàng.</p>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="service-item">
                        <img src="/assets/media/3-untitled-8.jpg" alt="">
                        
                        <h4>Cho thuê máy phát điện</h4>
                        <div class="row">
                            <div class="col-2">
                                <i class="flaticon-036-parking"></i>
                            </div>
                            <div class="col-10">
                                <p class="text-left">Công ty Hưng thịnh chuyên mua bán , sửa chữa, bảo trì, cho thuê tất cả các loại máy phát điện công nghiệp từ 15kva - 2500kva. Nhanh chóng giá rẻ tận nơi. có mặt nhanh chóng hỗ trợ khách hàng.</p>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="service-item">
                        <img src="/assets/media/3-untitled-8.jpg" alt="">
                        
                        <h4>Cho thuê máy phát điện</h4>
                        <div class="row">
                            <div class="col-2">
                                <i class="flaticon-036-parking"></i>
                            </div>
                            <div class="col-10">
                                <p class="text-left">Công ty Hưng thịnh chuyên mua bán , sửa chữa, bảo trì, cho thuê tất cả các loại máy phát điện công nghiệp từ 15kva - 2500kva. Nhanh chóng giá rẻ tận nơi. có mặt nhanh chóng hỗ trợ khách hàng.</p>
                            </div>
                        </div>
                        
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!-- Services Section End -->

    <!-- Home Room Section Begin -->
    <section class="hp-room-section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>Sản phẩm</h2>
                    </div>
                </div>
            </div>
            <div class="hp-room-items">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="hp-room-item set-bg" data-setbg="/assets/media/images/tmp/may-phat-dien-cong-nghiep-viet-nhat-82.jpg">
                            <div class="hr-text">
                                <h3>Máy phát điện 100kva</h3>
                                <!-- <h2>199$<span>/Pernight</span></h2> -->
                                <table>
                                    <tbody>
                                        <tr>
                                            <td class="r-o">Thông số 1:</td>
                                            <td>xxx</td>
                                        </tr>
                                        <tr>
                                            <td class="r-o">Thông số 2:</td>
                                            <td>xxx</td>
                                        </tr>
                                        <tr>
                                            <td class="r-o">Thông số 3:</td>
                                            <td>xxx</td>
                                        </tr>
                                        <tr>
                                            <td class="r-o">Thông số 4:</td>
                                            <td>xxx</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <a href="#" class="primary-btn">Xem chi tiết</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="hp-room-item set-bg" data-setbg="/assets/media/images/tmp/may-phat-dien-cong-nghiep-viet-nhat-82.jpg">
                            <div class="hr-text">
                                <h3>Máy phát điện 100kva</h3>
                                <!-- <h2>199$<span>/Pernight</span></h2> -->
                                <table>
                                    <tbody>
                                        <tr>
                                            <td class="r-o">Thông số 1:</td>
                                            <td>xxx</td>
                                        </tr>
                                        <tr>
                                            <td class="r-o">Thông số 2:</td>
                                            <td>xxx</td>
                                        </tr>
                                        <tr>
                                            <td class="r-o">Thông số 3:</td>
                                            <td>xxx</td>
                                        </tr>
                                        <tr>
                                            <td class="r-o">Thông số 4:</td>
                                            <td>xxx</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <a href="#" class="primary-btn">Xem chi tiết</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="hp-room-item set-bg" data-setbg="/assets/media/images/tmp/may-phat-dien-cong-nghiep-viet-nhat-82.jpg">
                            <div class="hr-text">
                                <h3>Máy phát điện 100kva</h3>
                                <!-- <h2>199$<span>/Pernight</span></h2> -->
                                <table>
                                    <tbody>
                                        <tr>
                                            <td class="r-o">Thông số 1:</td>
                                            <td>xxx</td>
                                        </tr>
                                        <tr>
                                            <td class="r-o">Thông số 2:</td>
                                            <td>xxx</td>
                                        </tr>
                                        <tr>
                                            <td class="r-o">Thông số 3:</td>
                                            <td>xxx</td>
                                        </tr>
                                        <tr>
                                            <td class="r-o">Thông số 4:</td>
                                            <td>xxx</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <a href="#" class="primary-btn">Xem chi tiết</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="hp-room-item set-bg" data-setbg="/assets/media/images/tmp/may-phat-dien-cong-nghiep-viet-nhat-82.jpg">
                            <div class="hr-text">
                                <h3>Máy phát điện 100kva</h3>
                                <!-- <h2>199$<span>/Pernight</span></h2> -->
                                <table>
                                    <tbody>
                                        <tr>
                                            <td class="r-o">Thông số 1:</td>
                                            <td>xxx</td>
                                        </tr>
                                        <tr>
                                            <td class="r-o">Thông số 2:</td>
                                            <td>xxx</td>
                                        </tr>
                                        <tr>
                                            <td class="r-o">Thông số 3:</td>
                                            <td>xxx</td>
                                        </tr>
                                        <tr>
                                            <td class="r-o">Thông số 4:</td>
                                            <td>xxx</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <a href="#" class="primary-btn">Xem chi tiết</a>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
    <!-- Home Room Section End -->

    <!-- Testimonial Section Begin -->
    <section class="testimonial-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <span><!-- Tin tức --></span>
                        <h2>Cảm nhận của khách hàng?</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="testimonial-slider owl-carousel">
                        <div class="ts-item">
                            <p>Tôi đang cần tìm mua máy phát điện và tìm trên mạng thấy công ty Việt Nhật tôi liên hệ để tư vấn và không ngần ngại chọn công ty vì phong cách phụ vụ rất chu đáo và tận tình. Cơ Điện Việt Nhật đã tư vấn cho tôi chọn được máy phù hợp và giảm thiểu được chi phí.</p>
                            <div class="ti-author">
                                <div class="rating">
                                    <i class="icon_star"></i>
                                    <i class="icon_star"></i>
                                    <i class="icon_star"></i>
                                    <i class="icon_star"></i>
                                    <i class="icon_star-half_alt"></i>
                                </div>
                                <h5> - Anh Đạt Giám đốc trại giam ở Quảng Ninh</h5>
                            </div>
                            <!-- <img src="img/testimonial-logo.png" alt=""> -->
                        </div>
                        <div class="ts-item">
                            <p>Mình chỉ có 2 từ ngắn gọn để nói về công ty cơ điện Việt Nhật là “quá tốt”. Mình ở Nha Trang và tìm trên mạng thấy công ty Việt Nhật chuyên cung cấp máy phát điện ở Sải Gòn thế là mình liên hệ để được tư vấn chọn máy. Nhân viên tư vấn rất tận tình và mình đã chọn máy ở Việt Nhật thế là cả đội ngủ nhân viên vận chuyển máy tận nơi lắp ráp cho mình mà không tín thêm bất kỳ chi phí gì.</p>
                            <div class="ti-author">
                                <div class="rating">
                                    <i class="icon_star"></i>
                                    <i class="icon_star"></i>
                                    <i class="icon_star"></i>
                                    <i class="icon_star"></i>
                                    <i class="icon_star-half_alt"></i>
                                </div>
                                <h5> - Chị Trịnh Thị Bích Thủy - Khách sạn Nhật Vy ở Nha Trang</h5>
                            </div>
                            <!-- <img src="img/testimonial- logo.png" alt=""> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Testimonial Section End -->

    <!-- Blog Section Begin -->
    <section class="blog-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <!-- <span>Hotel News</span> -->
                        <h2>Tin tức</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="blog-item set-bg" data-setbg="https://www.mayphatdiencongnghiep.info/wp-content/uploads/may-phat-dien-cu-viet-nhat-10.jpg">
                        <div class="bi-text">
                            <span class="b-tag">Tag</span>
                            <h4><a href="#">Cummins, ông hoàng của động cơ diesel !</a></h4>
                            <div class="b-time"><i class="icon_clock_alt"></i> 18/04/2019</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="blog-item set-bg" data-setbg="https://www.mayphatdiencongnghiep.info/wp-content/uploads/may-phat-dien-cu-viet-nhat-10.jpg">
                        <div class="bi-text">
                            <span class="b-tag">Tag</span>
                            <h4><a href="#">Cummins, ông hoàng của động cơ diesel !</a></h4>
                            <div class="b-time"><i class="icon_clock_alt"></i> 18/04/2019</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="blog-item set-bg" data-setbg="https://www.mayphatdiencongnghiep.info/wp-content/uploads/may-phat-dien-cu-viet-nhat-10.jpg">
                        <div class="bi-text">
                            <span class="b-tag">Tag</span>
                            <h4><a href="#">Cummins, ông hoàng của động cơ diesel !</a></h4>
                            <div class="b-time"><i class="icon_clock_alt"></i> 18/04/2019</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="blog-item set-bg" data-setbg="https://www.mayphatdiencongnghiep.info/wp-content/uploads/may-phat-dien-cu-viet-nhat-10.jpg">
                        <div class="bi-text">
                            <span class="b-tag">Tag</span>
                            <h4><a href="#">Cummins, ông hoàng của động cơ diesel !</a></h4>
                            <div class="b-time"><i class="icon_clock_alt"></i> 18/04/2019</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="blog-item set-bg" data-setbg="https://www.mayphatdiencongnghiep.info/wp-content/uploads/may-phat-dien-cu-viet-nhat-10.jpg">
                        <div class="bi-text">
                            <span class="b-tag">Tag</span>
                            <h4><a href="#">Cummins, ông hoàng của động cơ diesel !</a></h4>
                            <div class="b-time"><i class="icon_clock_alt"></i> 18/04/2019</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="blog-item set-bg" data-setbg="https://www.mayphatdiencongnghiep.info/wp-content/uploads/may-phat-dien-cu-viet-nhat-10.jpg">
                        <div class="bi-text">
                            <span class="b-tag">Tag</span>
                            <h4><a href="#">Cummins, ông hoàng của động cơ diesel !</a></h4>
                            <div class="b-time"><i class="icon_clock_alt"></i> 18/04/2019</div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!-- Blog Section End -->
@stop
