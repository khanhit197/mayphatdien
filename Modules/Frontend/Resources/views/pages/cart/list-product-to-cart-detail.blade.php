@if(count(Cart::content()))
@foreach (Cart::content() as $key => $value)
<?php $product = \Modules\Product\Entities\product::find($value->id);
 ?>
<tr class="cart__tr">
    <td class="cart__td cart__remove"><a class="cart__remove-button remove" href="#" data-rowId="{{ $value->rowId }}">×</a></td>
    <td class="cart__td"><a class="cart__img" href="{{ route('frontend.product.product.detail-product',$product->slug) }}"><img src="{{ $product->thumbnail }}" /></a></td>
    <td class="cart__td"><span>Sản phẩm:</span><a href="{{ route('frontend.product.product.detail-product',$product->slug) }}">{{ $product->name }}</a></td>
    <td class="cart__td"><span>Giá:</span>{{ formatPrice($value->price) }}</td>
    <td class="cart__td"><span>Số lượng:</span><input data-rowId="{{ $value->rowId }}" class="cart__quantity" type="number" size="{{ $value->qty }}" value="{{ $value->qty }}" /></td>
    <td class="cart__td"><span>Tổng:</span>{{ formatPrice($value->qty * $value->price) }}</td>
</tr>
@endforeach
@endif