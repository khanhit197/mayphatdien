@if(!Cart::count())
    <p class="cart-empty">Chưa có sản phẩm trong giỏ hàng.</p>
@else
	@if(count(Cart::content()))
	<ul class="cart-list">
	@foreach (Cart::content() as $key => $value)
	    <?php $product = \Modules\Product\Entities\Product::find($value->id);
	    ?>
	    <li class="cart-item"><a class="remove" data-rowId="{{ $value->rowId }}" href="#">×</a><a class="img" href="{{ route('frontend.product.product.detail-product',$product->slug) }}"><img src="{{ $product->thumbnail }}" />{{ $product->name }}</a>
	        <div class="quanlity">{{ $value->qty }} × <span class="amount">{{formatPrice((int)$value->price)}}<span class="currency">VND</span></span></div>
	    </li>
	@endforeach
	</ul>
	<p class="cart-total"><strong>Tổng phụ:</strong><span class="amount" id="total">{{formatPrice((int)Cart::total())}}<span class="currency">VND</span></span></p>
	<p class="cart-buttons"><a class="button" href="{{ route('frontend.cart.cart.cart-detail') }}">Xem giỏ hàng</a><a class="button checkout" href="{{ route('frontend.cart.cart.cart-address') }}">Thanh toán</a></p>
	@endif
@endif