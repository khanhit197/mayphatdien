@extends('frontend::frontend.layouts.master')
@section('content')
	<div class="breadcrumb">
        <div class="breadcrumb__container layout__container">
            <nav>
            	<a href="{{ route('frontend.home.home.home-page') }}">Trang chủ</a>
            	<span class="breadcrumb__separator">/</span>Giỏ hàng
            </nav>
        </div>
    </div>
	<div class="layout__spacing-menu layout__container">
        <main class="layout__main">
            <header class="title__header">
                <h1>Giỏ hàng </h1>
            </header>
            <div class="cart__content">
            	@if(!count(Cart::content()))
            	<p>Chưa có sản phẩm nào trong giỏ hàng.</p>
                <p><a class="button button__default" href="{{ route('frontend.product.product.store') }}">Quay trở lại cửa hàng</a></p>
                @else
				<table class="cart__table cart__table-list">
                    <thead>
                        <tr>
                            <th class="cart__th"> &nbsp</th>
                            <th class="cart__th"> &nbsp</th>
                            <th class="cart__th"> Sản phẩm</th>
                            <th class="cart__th"> Giá</th>
                            <th class="cart__th"> Số lượng</th>
                            <th class="cart__th"> Tổng</th>
                        </tr>
                    </thead>
                    <tbody id="cart__tbody">
                    	@if(count(Cart::content()))
		                @foreach (Cart::content() as $key => $value)
		                <?php $product = \Modules\Product\Entities\product::find($value->id);
		                 ?>
		                <tr class="cart__tr">
                            <td class="cart__td cart__remove"><a class="cart__remove-button remove" href="#" data-rowId="{{ $value->rowId }}">×</a></td>
                            <td class="cart__td"><a class="cart__img" href="{{ route('frontend.product.product.detail-product',$product->slug) }}"><img src="{{ $product->thumbnail }}" /></a></td>
                            <td class="cart__td"><span>Sản phẩm:</span><a href="{{ route('frontend.product.product.detail-product',$product->slug) }}">{{ $product->name }}</a></td>
                            <td class="cart__td"><span>Giá:</span>{{ formatPrice($value->price) }}</td>
                            <td class="cart__td"><span>Số lượng:</span><input data-rowId="{{ $value->rowId }}" class="cart__quantity" type="number" size="{{ $value->qty }}" value="{{ $value->qty }}" /></td>
                            <td class="cart__td"><span>Tổng:</span>{{ formatPrice($value->qty * $value->price) }}</td>
                        </tr>
		                @endforeach
		                @endif
                    </tbody>
                </table>
                <div class="cart__actions">
                    <div class="cart__coupon"><input id="voucher-code" class="input__default" type="text" placeholder="Mã ưu đãi" /><button id="add-voucher" class="button button__default cart__submit" type="submit">Áp dụng</button></div>
                    <div class="cart__submit"><button class="button button__default cart__update" type="submit" disabled="disabled">Cập nhật giỏ hàng</button></div>
                </div>
                <div class="cart__totals">
                    <h2 class="cart__totals-title">Tổng số lượng</h2>
                    <table class="cart__table">
                        <tbody>
                            <tr>
                                <th class="cart__th"> Tổng phụ</th>
                                <td class="cart__td" id="sub-total"> {{ Cart::subtotal(0) }} VND</td>
                            </tr>
                            <tr>
                                <th class="cart__th"> Giảm giá</th>
                                <td class="cart__td" id="total-discount"> - </td>
                            </tr>
                            <tr>
                                <th class="cart__th"> Tổng</th>
                                <td class="cart__td" id="total-price"> {{ Cart::subtotal(0) }} VND</td>
                            </tr>
                        </tbody>
                    </table><a href="{{ route('frontend.cart.cart.cart-address') }}" class="button cart__checkout">Tiến hành thanh toán </a>
                </div>
            	@endif
            </div>
        </main>
    </div>

@endsection
@section('js')
<script type="text/javascript">
	function addVoucher(code)
    {
        var form = new FormData();
            form.append('_token','<?= csrf_token() ?>');
            form.append('code',code);
            $.ajax({
                url: '{{route('frontend.cart.cart.add-voucher')}}',
                data: form,
                processData: false,
                contentType: false,
                type: 'POST',
                beforeSend:function(){
                    $("loading__container").addClass("active");
                },  
                success: function(data){
                    if(data.status == true){
                        $('#total-price').html(data.total);
                        $('#total-discount').html(data.discount);
                        sessionStorage.setItem("voucherCode", data.code);
                    }else{
                        alert(data.message);   
                    }
                    $(".loading__container").removeClass("active");
                    
                }
            });
    }
    function updateToCartPageDetail(arrRowId,arrQuantity)
    {
        var form = new FormData();
            form.append('_token','<?= csrf_token() ?>');
            form.append('arrRowId',arrRowId);
            form.append('arrQuantity',arrQuantity);
            $.ajax({
                url: '{{route('frontend.cart.cart.updateToCartPageCart')}}',
                data: form,
                processData: false,
                contentType: false,
                type: 'POST',
                beforeSend:function(){
                    $("loading__container").addClass("active");

                },  
                success: function(data){
                    $("#cart__tbody").html(data.data);
                    $('#total-price').html(data.total);
                    $('#sub-total').html(data.total);
                    sessionStorage.setItem("voucherCode", '');
                    $('#voucher-code').val('');
                    $('#total-discount').html('-');
                    $(".loading__container").removeClass("active");
                }
            });
    }

    sessionStorage.setItem("voucherCode", '');
    $("#add-voucher").on("click", function(e) {
        e.preventDefault();
        var code = $('#voucher-code').val().trim();
        addVoucher(code);
    });
    $('.cart__quantity').on('change', function(e) {
    	e.preventDefault();
    	$('.cart__update').prop("disabled", false);
    	var rowId = $(this).attr('data-rowId');
    	var val = $(this).val();
    });
    $('.cart__update').click(function(e) {
    	e.preventDefault();
        var arrVal = [];
        var arrRowID = [];
        $(".cart__tr .cart__quantity").each(function(item){
            var val = $(this).val().trim();
            var rowId = $(this).attr('data-rowId');
            arrVal.push(val);
            arrRowID.push(rowId);
        });
        updateToCartPageDetail(arrRowID,arrVal);
    });
</script>
@endsection