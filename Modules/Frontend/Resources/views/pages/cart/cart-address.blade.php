@extends('frontend::frontend.layouts.master')
@section('content')
    <input id="subtotal-hidden" type="hidden" value="{{ Cart::subtotal(0,'','') }}">
    <div class="breadcrumb">
        <div class="breadcrumb__container layout__container">
            <nav><a href="{{ route('frontend.home.home.home-page') }}">Trang chủ</a><span class="breadcrumb__separator">/</span>Thanh toán</nav>
        </div>
    </div>
    <div class="layout__container layout__spacing-menu">
        <header class="title__header">
            <h1>Thanh Toán</h1>
        </header>
        @if(!count(Cart::content()))
        @if(Session::has('orderSuccess') && Session::get('orderSuccess') == '1')
                <div class="noti__info"><span>Thanh toán thành công. <a style="float:right;background-color:#3D9CD2;border-left-width: 1px;border-left-style: solid;padding-left: 10px;" href="{{ route('frontend.product.product.store') }}" class="wc-forward">Tiếp tục mua hàng</a></span></div>
            @else
                <p>Chưa có sản phẩm nào trong giỏ hàng.</p>
                <p><a class="button button__default" href="{{ route('frontend.product.product.store') }}">Quay trở lại cửa hàng</a></p>
            @endif
        @else
        <div class="noti__info checkout__coupon-action"><span>Bạn đã có mã ưu đãi?</span><a href="#"> Ấn vào đây để nhập mã</a></div>
        <div class="checkout__coupon">
            <p>Nếu bạn có mã giảm giá, vui lòng điền vào phía bên dưới.</p>
            <div class="checkout__coupon-form">
                <p class="checkout__coupon-input"><input id="voucher-code" class="input__default" type="text" placeholder="Mã ưu đãi" /></p>
                <p class="checkout__coupon-submit"><button class="button button__default" type="button" id="add-voucher">Áp dụng</button></p>
            </div>
        </div>
        <ul class="noti__error" style="display: none;">
            <li style="display: none;" id="name-customer"> <strong> Tên </strong><span>là mục bắt buộc</span></li>
            <li style="display: none;" id="phone-number"> <strong>Số điện thoại </strong><span>là mục bắt buộc</span></li>
            <li style="display: none;" id="email"> <strong>Email </strong><span>là mục bắt buộc</span></li>
            <li style="display: none;" id="address"> <strong>Địa chỉ </strong><span>là mục bắt buộc</span></li>
        </ul>
        <div class="checkout__wrap">
            <div class="checkout__form">
                <h3>Thông tin thanh toán</h3>
                {!! Form::open(['route' => ['frontend.cart.cart.submit-order'], 'method' => 'post', 'id'=>"cart-address__form-container", 'class' => 'form__default']) !!}
                    <input type="hidden" name="voucher_code" id="voucher_code">
                    <input type="hidden" name="fee_ship" id="inputFeeShip">
                    <p class="form__group"><label> Tên <abbr class="required" title="bắt buộc">*</abbr></label><input id="val-name" name="customer_name" type="text" /></p>
                    <p class="form__group"><label> Tỉnh/ Thành Phố <abbr class="required" title="bắt buộc">*</abbr></label>
                        <select name="city_id" class="selectCity">
                            @foreach($cities as $city)
                            <option {{ $city->id == 79 ? 'selected' : '' }} value="{{ $city->id }}">{{ $city->name }}</option>
                            @endforeach
                        </select name="district_id"></p>
                    <p class="form__group"><label> Quận/ Huyện <abbr class="required" title="bắt buộc">*</abbr></label><select class="selectDistrict">
                            <option value="">Quận/ Huyện </option>
                        </select></p>
                    <p class="form__group"><label> Địa chỉ<abbr class="required" title="bắt buộc">*</abbr></label><input id="val-address" name="customer_address" type="text" placeholder="Địa chỉ" /></p>
                    <p class="form__group"><label> Số điện thoại <abbr class="required" title="bắt buộc">*</abbr></label><input id="val-phone-number" name="customer_phone" type="number" /></p>
                    <p class="form__group"><label> Địa chỉ email <abbr class="required" title="bắt buộc">*</abbr></label><input id="val-email" name="customer_email" type="text" /></p>
                    
                    <div class="checkout__note"><label> Ghi chú đơn hàng <span>(tuỳ chọn)</span></label><textarea class="input__default" rows="2" placeholder="Ghi chú về đơn hàng, ví dụ: thời gian hay chỉ dẫn địa điểm giao hàng chi tiết hơn."></textarea></div>
                {!! Form::close() !!}
            </div>
            <div class="checkout__review">
                <h3>Đơn hàng của bạn</h3>
                <table class="table__default">
                    <thead>
                        <tr>
                            <th>Sản phẩm</th>
                            <th>Tổng</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count(Cart::content()))
                        @foreach (Cart::content() as $key => $value)
                            <tr>
                                <td> {{ $value->name }} <strong>× {{ $value->qty }}</strong></td>
                                <td><span>{{ formatPrice($value->qty * $value->price) }}VND</span></td>
                            </tr>
                        @endforeach
                        @endif
                        
                    </tbody>
                    <tfoot>
                        <tr>
                            <th> Tổng phụ</th>
                            <td id="totalTemp"> {{ Cart::subtotal(0) }}VND</td>
                        </tr>
                        <tr>
                            <th> Giao hàng</th>
                            <td id="fee-ship"> 0VND</td>
                        </tr>
                        <tr>
                            <th> Tổng</th>
                            <td id="total-price"> {{ Cart::subtotal(0) }}VND</td>
                        </tr>
                    </tfoot>
                </table>
                <div class="checkout__payment-method"><button class="button button__dark checkout__submit" type="submit">Đặt hàng</button></div>
            </div>
        </div>
        @endif
    </div>
@endsection

@section('js')
<script type="text/javascript">
    function addVoucher(code)
    {
        var form = new FormData();
            form.append('_token','<?= csrf_token() ?>');
            form.append('code',code);
            $.ajax({
                url: '{{route('frontend.cart.cart.add-voucher')}}',
                data: form,
                processData: false,
                contentType: false,
                type: 'POST',
                beforeSend:function(){
                    $("loading__container").addClass("active");
                },  
                success: function(data){
                    if(data.status == true){
                        $('#total-price').html(data.total);
                        $('#total-discount').html(data.discount);
                        sessionStorage.setItem("voucherCode", data.code);
                        location.reload();
                    }else{
                        alert(data.message);   
                    }
                    $(".loading__container").removeClass("active");
                    
                }
            });
    }
    $(document).ready(function(){
        $("#add-voucher").on("click", function(e) {
            e.preventDefault();
            var code = $('#voucher-code').val().trim();
            addVoucher(code);
        });
        if(sessionStorage.getItem("voucherCode") ){
            $('#voucher_code').val(sessionStorage.getItem("voucherCode"));
        }

        $('.checkout__submit').on("click" , function(e){
            e.preventDefault();
            var status = true;
            var customer_name = $('#val-name').val();
            var phone = $('#val-phone-number').val().trim();
            var address = $('#val-address').val().trim();
            var email = $('#val-email').val().trim();

            if(customer_name.length == 0){
                status = false;
                $('#val-name').closest('.form__group').addClass('invalid');
                $("#name-customer").show();
                $(".noti__error").show();
            }else{
                $('#val-name').closest('.form__group').removeClass('invalid');
                $("#name-customer").hide();
                $(".noti__error").hide();
            }

            if(phone.length == 0){
                status = false;
                $('#val-phone-number').closest('.form__group').addClass('invalid');
                $("#phone-number").show();
                $(".noti__error").show();
            }else{
                $('#val-phone-number').closest('.form__group').removeClass('invalid');
                $("#phone-number").hide();
                $(".noti__error").hide();
            }

            if(address.length == 0){
                status = false;
                $('#val-address').closest('.form__group').addClass('invalid');
                $("#address").show();
                $(".noti__error").show();
            }else{
                $('#val-address').closest('.form__group').removeClass('invalid');
                $("#address").hide();
                $(".noti__error").hide();
            }

            if(email.length == 0){
                status = false;
                $('#val-email').closest('.form__group').addClass('invalid');
                $("#email").show();
                $(".noti__error").show();
            }else{
                $('#val-email').closest('.form__group').removeClass('invalid');
                $("#email").hide();
                $(".noti__error").hide();
            }
            if(status == true)
            {
                $('#cart-address__form-container').submit();
            }
            
        });
        $('.selectCity').on("change" , function(e){
            e.preventDefault();
            var id = $(".selectCity option:selected").val();
            var form = new FormData();
                form.append('_token','<?= csrf_token() ?>');
                form.append('id',id);
            $.ajax({
                url: '{{route('frontend.cart.cart.get-district')}}',
                data: form,
                processData: false,
                contentType: false,
                type: 'POST',
                beforeSend:function(){
                    $(".loading__container").addClass("active");
                },  
                success: function(data){
                    $('.selectDistrict').html(data);
                    $(".loading__container").removeClass("active");
                    $('.selectDistrict').trigger('change');
                    
                }
            });
        });
        $('.selectCity').trigger('change');
        $('body').on("change",'.selectDistrict' , function(e){
            e.preventDefault();
            var id = $(".selectDistrict option:selected").val();
            var form = new FormData();
                form.append('_token','<?= csrf_token() ?>');
                if(sessionStorage.getItem("voucherCode")){
                    var voucher_code = $('#voucher_code').val()
                    form.append('voucher_code',voucher_code);
                }
                form.append('id',id);
            $.ajax({
                url: '{{route('frontend.cart.cart.fee-ship')}}',
                data: form,
                processData: false,
                contentType: false,
                type: 'POST',
                beforeSend:function(){
                    $("loading__container").addClass("active");
                },  
                success: function(data){
                    $('#fee-ship').html(data.feeShip);
                    $('#inputFeeShip').val(data.feeShipNoFormat);
                    $('#totalTemp').html(data.totalDiscount);
                    $('#total-price').html(data.total);
                    $(".loading__container").removeClass("active");
                    
                }
            });
        });
    });
</script>
@endsection