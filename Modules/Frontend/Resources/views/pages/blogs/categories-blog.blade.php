@extends('frontend::frontend.layouts.master')
@section('content')

	<div class="breadcrumb">
        <div class="breadcrumb__container layout__container">
            <nav><a href="{{ route('frontend.home.home.home-page') }}">Trang chủ</a><span class="breadcrumb__separator">/</span>
                <a href="{{ route('frontend.blog.blog.list-blogs') }}">Blog</a><span class="breadcrumb__separator">/</span>
                <span class="breadcrumb__separator">/</span>{{ $category->name }}</nav>
        </div>
    </div>
    <div class="blog-detail">
        <div class="blog-detail__wrapper layout__container">
            <main class="blog-detail__main">
                <header>
                    <h1 class="page-title">Chuyên mục: {{ $category->name }}</h1> 
                </header>
                @if($blogs)
                    @foreach($blogs as $blog)
                        <article class="blog-detail__post">
                        <div class="blog-detail__header"><span class="posted-on">Ngày đăng <a class="link" href="#"><time class="date">{{ \Carbon\Carbon::parse($blog->created_at)->format('d/m/Y') }}</time></a></span>
                            <h1 class="ttl">{{ $blog->title }}</h1>
                        </div>
                        <aside class="blog-detail__meta">
                            <div class="author"><img class="avatar" src="../assets/images/avatar.png" />
                                <div class="label-txt">Viết bởi</div><a class="url">{{ $blog->author }}</a>
                            </div>
                            <div class="cat-links">
                                <div class="label-txt">Đăng bởi</div>
                                @foreach($blog->categoryBlog as $key => $categoriesBlog)
                                    {{ ($key != 0) ? ',&nbsp;' : '' }}
                                    <a class="tag" href="{{ route('frontend.blog.blog.get-categories-blog',$categoriesBlog->slug) }}">{{ $categoriesBlog->name }}</a>
                                @endforeach
                            </div>
                        </aside>
                        <div class="blog-detail__content">
                            {!! $blog->content !!}
                        </div>
                    </article>
                    @endforeach
                @endif
                <div class="c-pagination" style="border: 1px solid rgba(0,0,0,.05);border-width: 1px 0;text-align: center;">
                    <nav class="c-pagination__number">
                        {{ $blogs->links() }}
                    </nav>
                </div>
            </main>
        </div>
    </div>

@endsection
@section('js')
@endsection