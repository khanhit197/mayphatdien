@extends('frontend::frontend.layouts.master')
@section('content')

	<div class="breadcrumb">
        <div class="breadcrumb__container layout__container">
            <nav><a href="{{ route('frontend.home.home.home-page') }}">Trang chủ</a><span class="breadcrumb__separator">/</span>
                <a href="{{ route('frontend.blog.blog.list-blogs') }}">Blog</a><span class="breadcrumb__separator">/</span>
                @foreach($categoriesBlogs as $key => $categoriesBlog)
                    {{ ($key != 0) ? ',&nbsp;' : '' }}
                    <a class="tag" href="{{ route('frontend.blog.blog.get-categories-blog',$categoriesBlog->slug) }}">{{ $categoriesBlog->name }}</a>
                @endforeach
                <span class="breadcrumb__separator">/</span>{{ $blog->title }}</nav>
        </div>
    </div>
    <div class="blog-detail">
        <div class="blog-detail__wrapper layout__container">
            <main class="blog-detail__main">
                <article class="blog-detail__post">
                    <div class="blog-detail__header"><span class="posted-on">Ngày đăng <a class="link" href="#"><time class="date">{{ \Carbon\Carbon::parse($blog->created_at)->format('d/m/Y') }}</time></a></span>
                        <h1 class="ttl">{{ $blog->title }}</h1>
                    </div>
                    <aside class="blog-detail__meta">
                        <div class="author"><img class="avatar" src="../assets/images/avatar.png" />
                            <div class="label-txt">Viết bởi</div><a class="url">{{ $blog->author }}</a>
                        </div>
                        <div class="cat-links">
                            <div class="label-txt">Đăng bởi</div>
                            @foreach($categoriesBlogs as $key => $categoriesBlog)
                                {{ ($key != 0) ? ',&nbsp;' : '' }}
                                <a class="tag" href="{{ route('frontend.blog.blog.get-categories-blog',$categoriesBlog->slug) }}">{{ $categoriesBlog->name }}</a>
                            @endforeach
                        </div>
                    </aside>
                    <div class="blog-detail__content">
                        {!! $blog->content !!}
                    </div>
                    <nav class="c-pagination1">
                        <div class="c-pagination1__nav">
                            @if($previous)
                                <div class="c-pagination1__prev"><a href="{{ route('frontend.blog.blog.detail-blog',$previous->slug) }}"><span class="reader-text"></span>{{ $previous->title }}</a></div>
                            @endif
                            @if($next)
                                <div class="c-pagination1__next" style="margin-left: auto;"><a href="{{ route('frontend.blog.blog.detail-blog',$next->slug) }}"><span class="reader-text"></span>{{ $next->title }}</a></div>
                            @endif
                        </div>
                    </nav>
                </article>
            </main>
        </div>
    </div>

@endsection
@section('js')
@endsection