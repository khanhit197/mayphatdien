@extends('frontend::frontend.layouts.master')
@section('content')

	<div class="breadcrumb">
        <div class="breadcrumb">
        <div class="breadcrumb__container layout__container">
            <nav><a href="{{ route('frontend.home.home.home-page') }}">Trang chủ</a><span class="breadcrumb__separator">/</span>
                <a href="{{ route('frontend.blog.blog.list-blogs') }}">Blog</a>
                @if($category)
                    <span class="breadcrumb__separator">/</span>
                    <span class="breadcrumb__separator">/</span>{{ $category->name }}
                @endif
            </nav>
        </div>
    </div>
    </div>
    <div class="blog-list">
        <div class="blog-list__wrapper layout__container">
            <main class="blog-list__main">
                <article class="blog-list__post">
                    <h1 class="blog-list__ttl">{{ ($category) ? $category->name : 'Blog' }}</h1>
                    <div class="blog-list__content">
                        @foreach($blogs as $key => $blog)
                            @if($key % 2 == 0)
                                <div class="c-card1">
                                    <div class="c-card1__left">
                                        <div class="wrapper">
                                            <figure><a class="link" href="{{ route('frontend.blog.blog.detail-blog',$blog->slug) }}"><img class="img" src="{{ $blog->image }}" alt="" /></a>
                                                <div class="share-image"><a href="#" target="_blank"></a></div>
                                            </figure>
                                            <div class="read-more"><a class="more-tag" href="{{ route('frontend.blog.blog.detail-blog',$blog->slug) }}">Read More</a></div>
                                        </div>
                                    </div>
                                    <div class="c-card1__right">
                                        <h2 class="ttl"><a class="link" href="{{ route('frontend.blog.blog.detail-blog',$blog->slug) }}">{{ $blog->title }}</a></h2>
                                        <div class="content">{!! $blog->descriptions !!}</div>
                                    </div>
                                </div>
                            @else
                                <div class="c-card1 c-card1--reverse">
                                    <div class="c-card1__left">
                                        <div class="wrapper">
                                            <figure><a class="link" href="{{ route('frontend.blog.blog.detail-blog',$blog->slug) }}"><img class="img" src="{{ $blog->image }}" alt="" /></a>
                                                <div class="share-image"><a href="#" target="_blank"></a></div>
                                            </figure>
                                            <div class="read-more"><a class="more-tag" href="{{ route('frontend.blog.blog.detail-blog',$blog->slug) }}">Read More</a></div>
                                        </div>
                                    </div>
                                    <div class="c-card1__right">
                                        <h2 class="ttl"><a class="link" href="{{ route('frontend.blog.blog.detail-blog',$blog->slug) }}">{{ $blog->title }}</a></h2>
                                        <div class="content">{!! $blog->descriptions !!}</div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                        <div class="c-pagination">
                            <nav class="c-pagination__number">
                                {{ $blogs->links() }}
                            </nav>
                        </div>
                    </div>
                </article>
            </main>
        </div>
    </div>

@endsection
@section('js')
@endsection