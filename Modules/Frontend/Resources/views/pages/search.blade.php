@extends('frontend::frontend.layouts.master')
@section('content')
<div class="breadcrumb">
        <div class="breadcrumb__container layout__container">
            <nav><a href="#">Trang chủ</a><span class="breadcrumb__separator">/</span>Cửa Hàng</nav>
        </div>
    </div>
    <div class="store">
        <div class="store__wrapper layout__container">
            <div class="store__primary" style="width:100%">
                <main class="store__main layout__main">
                    <div class="title__header">
                        <h1>Kết quả tìm kiếm: {{ $request->keyword }}</h1>
                    </div>
                    <div class="c-sort">
                        <input type="hidden" value="{{ $request->keyword }}" id="get-request">
                        <form class="ordering" action="" method="get"><select class="orderby">
                                <option {{ ($request->order == 'populariry') ? 'selected' : '' }} value="populariry">Thứ tự theo mức độ phổ biến</option>
                                <option {{ ($request->order == 'date') ? 'selected' : '' }} value="date">Mới nhất</option>
                                <option {{ ($request->order == 'price') ? 'selected' : '' }} value="price">Thứ tự theo giá: thấp đến cao</option>
                                <option {{ ($request->order == 'price-desc') ? 'selected' : '' }} value="price-desc">Thứ tự theo giá: cao xuống thấp</option>
                            </select><input type="hidden" /></form>
                        <p class="count">Hiển thị {{ ($products->lastItem() > 1) ? $products->firstItem() . ' - ' . $products->lastItem() : $products->firstItem() }} {{ $products->total() > 1 ?  'trong ' . $products->total() : ''}} kết quả</p>
                        {{ $products->render('frontend::frontend.partials.paginator') }}
                    </div>
                    <div class="store__col4">
                        <ul class="c-list1">
                            @if($products)
                                @foreach($products as $product)
                                    <li class="c-list1__item"><a class="c-list1__img" href="{{ route('frontend.product.product.detail-product',$product->slug) }}"><img src="{{ $product->thumbnail }}" /></a>
                                        <h3 class="c-list1__brand"><a href="{{ route('frontend.trademark.trademark.get-products',$product->trademark->slug) }}">{{ $product->trademark->name }}</a></h3>
                                        <h2 class="c-list1__ttl">{{ $product->name }}</h2>
                                        @if($product->discount)
                                            <span class="c-list1__onsale">{{ $product->discount }}%</span>
                                            <span class="c-list1__price"><del><span class="amount">{{ formatPrice($product->price) }}<span class="currency">VND</span></span></del><ins><span class="amount">{{ priceDiscount($product->price,$product->discount) }}<span class="currency">VND</span></ins></span></span>
                                        @else
                                            <span class="c-list1__price"><span class="amount">{{ formatPrice($product->price) }}<span class="currency">VND</span></span></span>
                                        @endif
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                    <div class="c-sort">
                        <form class="ordering" action="" method="get"><select class="orderby">
                                <option {{ ($request->order == 'populariry') ? 'selected' : '' }} value="populariry">Thứ tự theo mức độ phổ biến</option>
                                <option {{ ($request->order == 'date') ? 'selected' : '' }} value="date">Mới nhất</option>
                                <option {{ ($request->order == 'price') ? 'selected' : '' }} value="price">Thứ tự theo giá: thấp đến cao</option>
                                <option {{ ($request->order == 'price-desc') ? 'selected' : '' }} value="price-desc">Thứ tự theo giá: cao xuống thấp</option>
                            </select><input type="hidden" /></form>
                        <p class="count">Hiển thị {{ ($products->lastItem() > 1) ? $products->firstItem() . ' - ' . $products->lastItem() : $products->firstItem() }} {{ $products->total() > 1 ?  'trong ' . $products->total() : ''}} kết quả</p>
                        {{ $products->render('frontend::frontend.partials.paginator') }}
                    </div>
                </main>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script type="text/javascript">
    $(".orderby").change(function (event) {                
        var type = $(this).val();   
        var val = $('#get-request').val();   
        var currentURL = window.location.origin + window.location.pathname;
        var url = currentURL+ '?' + '&keyword=' + val + '&order=' + type; 
        window.location.href = url;
    });
</script>
@endsection