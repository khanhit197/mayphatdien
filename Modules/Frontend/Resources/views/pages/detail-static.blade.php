@extends('frontend::frontend.layouts.master')
@section('content')
	
	<div class="breadcrumb">
        <div class="breadcrumb__container layout__container">
            <nav><a href="{{ route('frontend.home.home.home-page') }}">Trang chủ</a><span class="breadcrumb__separator">/</span>{{ $temp->title }}</nav>
        </div>
    </div>
	<div class="layout__container layout__spacing-menu" style="margin-bottom: 100px;">
        <h1>{{ $temp->title }}</h1>
        {!! $temp->content !!}
    </div>

@endsection