@extends('frontend::frontend.layouts.master')
@section('content')
	<div class="breadcrumb">
        <div class="breadcrumb__container layout__container">
            <nav>
            	<a href="{{ route('frontend.home.home.home-page') }}">Trang chủ</a>
            	<span class="breadcrumb__separator">/</span><a class="tag" href="{{ route('frontend.trademark.trademark.index') }}">Thương hiệu</a>
            	<span class="breadcrumb__separator">/</span>{{ $trademark->name }}</nav>
            </nav>
        </div>
    </div>
	<div class="store">
        <div class="store__wrapper layout__container">
            <div class="store__secondary">
                <div class="store__area"><span class="ttl">Danh mục sản phẩm</span>
                    <div class="c-select1">
                        <select class="c-select1__list" id="categories">
                            <option selected="selected" disabled="disabled" value="">Chọn danh mục</option>
                            @if($categories)
                                @foreach($categories as $category)
                                <option {{($category->slug == $request->filter_category) ? 'selected' : ''}} value="{{ $category->slug }}">{{ $category->name }}</option>
                                @endforeach
                            @endif
                        </select>
                        <div class="c-select1__arrow"><i></i></div>
                    </div>
                </div>
                <div class="store__area"><span class="ttl">Lọc theo giá</span>
                    <form action="" method="get">
                        <div class="price-slide" id="slider-range" data-price-min="{{ $request->min_price ? $request->min_price : '150000' }}" data-price-max="{{ $request->max_price ? $request->max_price : '29900000' }}"></div>
                        <div class="price-slide-amount"><input name="min_price" type="number" id="price-filter-min"/><input name="max_price" type="number" id="price-filter-max"/><button class="button" type="button" id="btn-filter">Lọc</button>
                            <div class="price-label">Giá <label class="from" id="form">150,000</label> — <span class="to" id="to">29,900,000</span></div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="store__primary">
                <main class="store__main">
                    <div class="store__header" style="text-align: center;">
                    	<img src="{{ ($trademark->logo) ? $trademark->logo : '\assets\images\home\default.jpg'}}" alt="{{ $trademark->name }}" class="brand-thumbnail" width="324" height="432" style="max-height: 2.2906835em;margin: 0 0 1em;width: auto;align-self: center;-webkit-box-ordinal-group: 2;order: 1;">
                        <h1 class="ttl">{{ $trademark->name }}</h1>
                        <div class="desc">
                            {!! $trademark->descriptions !!}
                        </div>
                    </div>
                    @if(count($products))
                    <div class="c-sort">
                        <form class="ordering" action="#" method="get"><select class="orderby">
                                <option {{ ($request->order == 'populariry') ? 'selected' : '' }} value="populariry">Thứ tự theo mức độ phổ biến</option>
                                <option {{ ($request->order == 'date') ? 'selected' : '' }} value="date">Mới nhất</option>
                                <option {{ ($request->order == 'price') ? 'selected' : '' }} value="price">Thứ tự theo giá: thấp đến cao</option>
                                <option {{ ($request->order == 'price-desc') ? 'selected' : '' }} value="price-desc">Thứ tự theo giá: cao xuống thấp</option>
                            </select><input type="hidden" /></form>
                        <p class="count">Hiển thị {{ ($products->lastItem() > 1) ? $products->firstItem() . ' - ' . $products->lastItem() : $products->firstItem() }} {{ $products->total() > 1 ?  'trong ' . $products->total() : ''}} kết quả</p>
                        {{ $products->render('frontend::frontend.partials.paginator') }}
                    </div>
                    <div class="store__col4">
                        <ul class="c-list1">
                            @foreach($products as $product)
                                <li class="c-list1__item"><a class="c-list1__img" href="{{ route('frontend.product.product.detail-product',$product->slug) }}"><img src="{{ $product->thumbnail }}" /></a>
                                    <h3 class="c-list1__brand"><a href="{{ route('frontend.trademark.trademark.get-products',$product->trademark->slug) }}">{{ $product->trademark->name }}</a></h3>
                                    <h2 class="c-list1__ttl">{{ $product->name }}</h2>
                                    @if($product->discount)
                                        <span class="c-list1__onsale">{{ $product->discount }}%</span>
                                        <span class="c-list1__price"><del><span class="amount">{{ formatPrice($product->price) }}<span class="currency">VND</span></span></del><ins><span class="amount">{{ priceDiscount($product->price,$product->discount) }}<span class="currency">VND</span></ins></span></span>
                                    @else
                                        <span class="c-list1__price"><span class="amount">{{ formatPrice($product->price) }}<span class="currency">VND</span></span></span>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="store__sort">
                        <form class="ordering" action="#" method="get"><select class="orderby">
                                <option {{ ($request->order == 'populariry') ? 'selected' : '' }} value="populariry">Thứ tự theo mức độ phổ biến</option>
                                <option {{ ($request->order == 'date') ? 'selected' : '' }} value="date">Mới nhất</option>
                                <option {{ ($request->order == 'price') ? 'selected' : '' }} value="price">Thứ tự theo giá: thấp đến cao</option>
                                <option {{ ($request->order == 'price-desc') ? 'selected' : '' }} value="price-desc">Thứ tự theo giá: cao xuống thấp</option>
                            </select><input type="hidden" /></form>
                        <p class="count">Hiển thị {{ ($products->lastItem() > 1) ? $products->firstItem() . ' - ' . $products->lastItem() : $products->firstItem() }} {{ $products->total() > 1 ?  'trong ' . $products->total() : ''}} kết quả</p>
                        {{ $products->render('frontend::frontend.partials.paginator') }}
                    </div>
                    @else
                        <div class="noti__info checkout__coupon-action"><span>Không tìm thấy sản phẩm nào khớp với lựa chọn của bạn. </span></div>
                    @endif
                </main>
            </div>
        </div>
    </div>

@endsection
@section('js')
<script type="text/javascript">
    $(".orderby").change(function (event) {                
        var type = $(this).val();   
        var filter_category = $("#categories option:selected").val();
        var priceMin = $('#price-filter-min').val();
        var priceMax = $('#price-filter-max').val();
        var currentURL = window.location.origin + window.location.pathname;
        var url = currentURL+'?'+'&filter_category=' + filter_category+'&order=' + type+'&min_price=' + priceMin+'&max_price=' + priceMax; 
        window.location.href = url;
    });
    $("#categories").change(function (event) {                
        var filter_category = $(this).val();  
        var type = $(".orderby option:selected").val();
        var priceMin = $('#price-filter-min').val();
        var priceMax = $('#price-filter-max').val();
        var currentURL = window.location.origin + window.location.pathname;
        var url = currentURL+'?'+'&filter_category=' + filter_category+'&order=' + type+'&min_price=' + priceMin+'&max_price=' + priceMax; 
        window.location.href = url;
    });
    $("#btn-filter").click(function(event) {
        event.preventDefault();
        var priceMin = $('#price-filter-min').val();
        var priceMax = $('#price-filter-max').val();
        var type = $(".orderby option:selected").val();
        var filter_category = $("#categories option:selected").val();
        var currentURL = window.location.origin + window.location.pathname;
        var url = currentURL+'?'+'&filter_category=' + filter_category+'&order=' + type+'&min_price=' + priceMin+'&max_price=' + priceMax; 
        window.location.href = url;
    });
</script>
@endsection