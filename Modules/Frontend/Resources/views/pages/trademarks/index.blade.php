@extends('frontend::frontend.layouts.master')
@section('content')

	<div class="breadcrumb">
        <div class="breadcrumb__container layout__container">
            <nav><a href="#">Trang chủ</a><span class="breadcrumb__separator">/</span>Thương Hiệu</nav>
        </div>
    </div>
    <div class="brand">
        <div class="brand__wrapper layout__container">
            <main class="brand__main">
                <article class="brand__post">
                    <h1 class="brand__ttl">Thương Hiệu</h1>
                    <div class="brand__content">
                        <div id="brands_a_z">
                            <ul class="brand__index">
                                <li><a href="#">a</a></li>
                                <li><a href="#">b</a></li>
                                <li><a href="#">c</a></li>
                                <li><a href="#">d</a></li>
                                <li><span>e</span></li>
                                <li><a href="#">f</a></li>
                                <li><a href="#">g</a></li>
                                <li><span>h</span></li>
                                <li><span>i</span></li>
                                <li><span>j</span></li>
                                <li><span>k</span></li>
                                <li><a href="#">l</a></li>
                                <li><a href="#">m</a></li>
                                <li><a href="#">n</a></li>
                                <li><a href="#">o</a></li>
                                <li><span>p</span></li>
                                <li><span>q</span></li>
                                <li><a href="#">r</a></li>
                                <li><a href="#">s</a></li>
                                <li><span>t</span></li>
                                <li><span>u</span></li>
                                <li><span>v</span></li>
                                <li><span>w</span></li>
                                <li><a href="#">x</a></li>
                                <li><span>y</span></li>
                                <li><span>z</span></li>
                                <li><span>0-9</span></li>
                            </ul>
                            <div class="brand__brands">
                                <h3 id="brand-a">a</h3>
                                <ul class="brands">
                                    <li><a href="#">Adidas</a></li>
                                    <li><a href="#">Air Jordan</a></li>
                                    <li><a href="#">Alexander Mcqueen</a></li>
                                    <li><a href="#">Anti Social Social Club</a></li>
                                </ul><a class="top" id="brands_a_z">↑ Top</a>
                                <h3 id="brand-a">b</h3>
                                <ul class="brands">
                                    <li><a href="#">Balenciaga</a></li>
                                    <li><a href="#">Bottega Veneta</a></li>
                                </ul><a class="top" id="brands_a_z">↑ Top</a>
                                <h3 id="brand-a">c</h3>
                                <ul class="brands">
                                    <li><a href="#">Champion</a></li>
                                    <li><a href="#">Christian Louboutin</a></li>
                                    <li><a href="#">Comme Des Garçons</a></li>
                                </ul><a class="top" id="brands_a_z">↑ Top</a>
                                <h3 id="brand-a">d</h3>
                                <ul class="brands">
                                    <li><a href="#">Dior</a></li>
                                    <li><a href="#">Dolce & Gabbana</a></li>
                                    <li><a href="#">DRKSHDW</a></li>
                                </ul><a class="top" id="brands_a_z">↑ Top</a>
                                <h3 id="brand-a">f</h3>
                                <ul class="brands">
                                    <li><a href="#">Fendi</a></li>
                                </ul><a class="top" id="brands_a_z">↑ Top</a>
                                <h3 id="brand-a">g</h3>
                                <ul class="brands">
                                    <li><a href="#">Givenchy</a></li>
                                    <li><a href="#">Gucci</a></li>
                                </ul><a class="top" id="brands_a_z">↑ Top</a>
                                <h3 id="brand-a">l</h3>
                                <ul class="brands">
                                    <li><a href="#">Louis Vuitton</a></li>
                                </ul><a class="top" id="brands_a_z">↑ Top</a>
                                <h3 id="brand-a">m</h3>
                                <ul class="brands">
                                    <li><a href="#">Marcelo Burlon</a></li>
                                    <li><a href="#">Moschino</a></li>
                                </ul><a class="top" id="brands_a_z">↑ Top</a>
                                <h3 id="brand-a">n</h3>
                                <ul class="brands">
                                    <li><a href="#">Nike</a></li>
                                </ul><a class="top" id="brands_a_z">↑ Top</a>
                                <h3 id="brand-a">o</h3>
                                <ul class="brands">
                                    <li><a href="#">Off-White</a></li>
                                </ul><a class="top" id="brands_a_z">↑ Top</a>
                                <h3 id="brand-a">r</h3>
                                <ul class="brands">
                                    <li><a href="#">Re-Deadstock</a></li>
                                </ul><a class="top" id="brands_a_z">↑ Top</a>
                                <h3 id="brand-a">s</h3>
                                <ul class="brands">
                                    <li><a href="#">Saint Laurent</a></li>
                                    <li><a href="#">Street Icon</a></li>
                                    <li><a href="#">Stussy</a></li>
                                    <li><a href="#">Supreme</a></li>
                                </ul><a class="top" id="brands_a_z">↑ Top</a>
                                <h3 id="brand-a">x</h3>
                                <ul class="brands">
                                    <li><a href="#">Xvessel</a></li>
                                </ul><a class="top" id="brands_a_z">↑ Top</a>
                            </div>
                        </div>
                    </div>
                </article>
            </main>
        </div>
    </div>

@endsection
@section('js')
@endsection