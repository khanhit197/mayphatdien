@extends('frontend::frontend.layouts.master')
@section('content')
	<div class="breadcrumb">
        <div class="breadcrumb__container layout__container">
            <nav><a href="{{ route('frontend.home.home.home-page') }}">Trang chủ</a><span class="breadcrumb__separator">/</span>Cửa hàng</nav>
        </div>
    </div>
    <div class="store">
        <div class="store__wrapper layout__container">
            <div class="store__secondary">
                <div class="store__area"><span class="ttl">Thương hiệu</span>
                    <div class="c-select1">
                        <select name="filter_brand" class="c-select1__list" id="trademark">
                            <option selected="selected" disabled="disabled" value="">Chọn thương hiệu</option>
                            @if($trademarks)
                                @foreach($trademarks as $trademark)
                                    <option {{ ($request->filter_brand ==  $trademark->slug) ? 'selected' : ''}} value="{{ $trademark->slug }}">{{ $trademark->name }}</option>
                                @endforeach
                            @endif
                        </select>
                        <div class="c-select1__arrow"><i></i></div>
                    </div>
                </div>
                <div class="store__area"><span class="ttl">Lọc theo giá</span>
                    <form action="" method="get">
                        <div class="price-slide" id="slider-range" data-price-min="150000" data-price-max="29900000"></div>
                        <div class="price-slide-amount"><input name="min_price" type="number" id="price-filter-min"/><input name="max_price" type="number" id="price-filter-max"/><button class="button" type="button" id="btn-filter">Lọc</button>
                            <div class="price-label">Giá <label class="from" id="form">150,000</label> — <span class="to" id="to">29,900,000</span></div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="store__primary">
                <main class="store__main layout__main">
                    <div class="store__header">
                        <h1 class="ttl">Cửa hàng</h1>
                    </div>
                    <div class="c-sort">
                        <form class="ordering" action="#" method="get"><select class="orderby">
                                <option {{ ($request->order == 'populariry') ? 'selected' : '' }} value="populariry">Thứ tự theo mức độ phổ biến</option>
                                <option {{ ($request->order == 'date') ? 'selected' : '' }} value="date">Mới nhất</option>
                                <option {{ ($request->order == 'price') ? 'selected' : '' }} value="price">Thứ tự theo giá: thấp đến cao</option>
                                <option {{ ($request->order == 'price-desc') ? 'selected' : '' }} value="price-desc">Thứ tự theo giá: cao xuống thấp</option>
                            </select><input type="hidden" /></form>
                        <p class="count">Hiển thị {{ ($stores->lastItem() > 1) ? $stores->firstItem() . ' - ' . $stores->lastItem() : $stores->firstItem() }} {{ $stores->total() > 1 ?  'trong ' . $stores->total() : ''}} kết quả</p>
                        {{ $stores->appends(['order' => $request->order,'filter_brand' => $request->filter_brand,'min_price' => $request->min_price,'max_price' => $request->max_price])->render('frontend::frontend.partials.paginator') }}
                    </div>
                    <div class="store__col4">
                        <ul class="c-list1">
                        	@if($stores)
                        		@foreach($stores as $product)
                        			<li class="c-list1__item"><a class="c-list1__img" href="{{ route('frontend.product.product.detail-product',$product->slug) }}"><img src="{{ $product->thumbnail }}" /></a>
		                                <h3 class="c-list1__brand"><a href="{{ route('frontend.trademark.trademark.get-products',$product->trademark->slug) }}">{{ $product->trademark->name }}</a></h3>
		                                <h2 class="c-list1__ttl">{{ $product->name }}</h2>
                                        @if($product->discount)
                                            <span class="c-list1__onsale">{{ $product->discount }}%</span>
                                            <span class="c-list1__price"><del><span class="amount">{{ formatPrice($product->price) }}<span class="currency">VND</span></span></del><ins><span class="amount">{{ priceDiscount($product->price,$product->discount) }}<span class="currency">VND</span></ins></span></span>
                                        @else
                                            <span class="c-list1__price"><span class="amount">{{ formatPrice($product->price) }}<span class="currency">VND</span></span></span>
                                        @endif
		                            </li>
                        		@endforeach
                        	@endif
                        </ul>
                    </div>
                    <div class="c-sort">
                        <form class="ordering" action="#" method="get"><select class="orderby">
                                <option {{ ($request->order == 'populariry') ? 'selected' : '' }} value="populariry">Thứ tự theo mức độ phổ biến</option>
                                <option {{ ($request->order == 'date') ? 'selected' : '' }} value="date">Mới nhất</option>
                                <option {{ ($request->order == 'price') ? 'selected' : '' }} value="price">Thứ tự theo giá: thấp đến cao</option>
                                <option {{ ($request->order == 'price-desc') ? 'selected' : '' }} value="price-desc">Thứ tự theo giá: cao xuống thấp</option>
                            </select><input type="hidden" /></form>
                        <p class="count">Hiển thị {{ ($stores->lastItem() > 1) ? $stores->firstItem() . ' - ' . $stores->lastItem() : $stores->firstItem() }} {{ $stores->total() > 1 ?  'trong ' . $stores->total() : ''}} kết quả</p>
                        {{ $stores->appends(['order' => $request->order,'filter_brand' => $request->filter_brand,'min_price' => $request->min_price,'max_price' => $request->max_price])->render('frontend::frontend.partials.paginator') }}
                    </div>
                </main>
            </div>
        </div>
    </div>

@endsection
@section('js')
<script type="text/javascript">
    $(".orderby").change(function (event) {                
        var type = $(this).val();   
        var filter_brand = $("#trademark option:selected").val();
        var priceMin = $('#price-filter-min').val();
        var priceMax = $('#price-filter-max').val();
        var currentURL = window.location.origin + window.location.pathname;
        var url = currentURL+'?'+'&filter_brand=' + filter_brand+'&order=' + type+'&min_price=' + priceMin+'&max_price=' + priceMax; 
        window.location.href = url;
    });
    $("#trademark").change(function (event) {                
        var filter_brand = $(this).val();  
        var type = $(".orderby option:selected").val();
        var priceMin = $('#price-filter-min').val();
        var priceMax = $('#price-filter-max').val();
        var currentURL = window.location.origin + window.location.pathname;
        var url = currentURL+'?'+'&filter_brand=' + filter_brand+'&order=' + type+'&min_price=' + priceMin+'&max_price=' + priceMax; 
        window.location.href = url;
    });
    $("#btn-filter").click(function(event) {
        event.preventDefault();
        var priceMin = $('#price-filter-min').val();
        var priceMax = $('#price-filter-max').val();
        var type = $(".orderby option:selected").val();
        var filter_brand = $("#trademark option:selected").val();
        var currentURL = window.location.origin + window.location.pathname;
        var url = currentURL+'?'+'&filter_brand=' + filter_brand+'&order=' + type+'&min_price=' + priceMin+'&max_price=' + priceMax; 
        window.location.href = url;
    });
</script>
@endsection