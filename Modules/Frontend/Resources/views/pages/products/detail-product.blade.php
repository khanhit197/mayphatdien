@extends('frontend::frontend.layouts.master')
@section('content')

	<div class="breadcrumb">
        <div class="breadcrumb__container layout__container">
            <nav>
            	<a href="{{ route('frontend.home.home.home-page') }}">Trang chủ</a>
            	<span class="breadcrumb__separator">/</span>{{ $product->name }}
            </nav>
        </div>
    </div>
    <div class="layout__container layout__spacing-menu">
        <div style="display: none;" id="add-success" class="noti__info"><span>“{{ $product->name }}” thêm vào giỏ hàng thành công. <a style="float:right;background-color:#3D9CD2;border-left-width: 1px;border-left-style: solid;padding-left: 10px;" href="{{ route('frontend.cart.cart.cart-detail') }}" class="wc-forward">Xem giỏ hàng</a></span></div>
            <div style="display: none;" class="noti__error checkout__coupon-action"><span>Vui lòng chọn thuộc tính của sản phẩm. </span></div>
        <div class="product-detail__wrap">
            
            <div class="product-detail__gallery"><a class="product-detail__gallery-action" href="#"></a>
                <div class="product-detail__gallery-view">
                    <div><img src="{{ $product->thumbnail }}" data-large_image="{{ $product->thumbnail }}" data-large_image_width="1000" data-large_image_height="500" title="test" /></div>
                    @if(count($gallerys))
                    	@foreach($gallerys as $gallery)
                        <div><img src="{{ $gallery }}" data-large_image="{{ $gallery }}" data-large_image_width="1000" data-large_image_height="500" title="test" /></div>
                        @endforeach
                    @endif
                </div>
                <ul class="product-detail__gallery-list">
                    <li class="active"><img src="{{ $product->thumbnail }}" /></li>
                    @if(count($gallerys))
                        @foreach($gallerys as $key => $gallery)
                        <li><img src="{{ $gallery }}" /></li>
                        @endforeach
                    @endif
                </ul>
            </div>
            <div class="product-detail__summary">
                <a href="{{ route('frontend.trademark.trademark.get-products',$product->trademark->slug) }}" class="product-detail__brands">{{ $product->trademark->name }}
                    @if($product->discount)
                    &emsp;<span class="c-list1__onsale">{{ $product->discount }}%</span>
                    @endif
                </a>
                <h1>{{ $product->name }}</h1>
                @if($product->discount)
                    <span class="product-detail__price"><del><span class="amount">{{ formatPrice($product->price) }}<span class="currency">VND</span></span></del>&emsp;<span class="amount">{{ priceDiscount($product->price,$product->discount) }}<span class="currency">VND</span></span></span>
                @else
                    <p class="product-detail__price">{{ formatPrice($product->price) }}<span class="currency">VND</span></p>
                @endif
                <div class="product-detail__description">
                    {!! $product->information !!}
                </div>
                
            	<div class="product-detail__size">
            		@if(count($product->productAttr))
                    <div class="form__group"><label>{{ $product->productAttr[0]->attribute->name }}</label>
                    	<select id="attribute">
                    		<option value="">Chọn một tùy chọn</option>
                    		@foreach($product->productAttr as $attr)
                            <option value="{{ $attr->id }}" {{ ($attr->status) ? 'data-stock=' . $attr->id : ''  }}>{{ $attr->value }}</option>
                            @endforeach
                        </select><a href="#" style="display:none">Xóa</a></div>
                    <p class="product-detail__stock-status">
                    	<span class="product-detail__instock">Còn hàng</span>
                    	<span class="product-detail__out-of-stock">Hết hàng!</span>
                    </p>
                    @else
	                    {!! ($product->status) ? '<span class="product-detail__instock">Còn hàng</span>' : '<span class="product-detail__out-of-stock">Hết hàng!</span>'!!}
	                @endif
                </div>
                @if($product->status)
                	<div class="product-detail__quantity"><input min="1" class="input__default" type="number" id="quantity" value="1" /><button class="button button__dark button-add-to-cart" data-product-id="{{ $product->id }}" type="submit">Thêm vào giỏ hàng</button></div>
                @endif
                <div class="product-detail__meta">
                	<span>Danh mục: 
                		@foreach($product->categories as $category)
                			<a href="{{ route('frontend.product.product.product-sub-category',[$parentCategory->slug,$category->slug]) }}">{{ $category->name }}</a>,&nbsp;
                		@endforeach
                	</span>
                	<span>Brand: <a href="{{ route('frontend.trademark.trademark.get-products',$product->trademark->slug) }}" class="product-detail__brands">{{ $product->trademark->name }}</a></span>
                </div>
            </div>
        </div>
        <div class="product-detail__tabs-wrap">
            <ul class="product-detail__tabs">
                <li class="active"><a href="#tab1">Mô tả</a></li>
                @if(count($product->productAttr))
                <li><a href="#tab2">Thông tin bổ sung</a></li>
                @endif
            </ul>
            <div class="product-detail__content-wrap">
                <div class="product-detail__content" id="tab1" style="display:block">
                    {!! $product->description !!}
                </div>
                @if(count($product->productAttr))
                <div class="product-detail__content" id="tab2">
                    <table class="table__default">
                        <tbody>
                            <tr>
                                <th>SIZE</th>
                                <td> 
                                	@foreach($product->productAttr as $key => $item)
                                	{{ $key != 0 ? ',&nbsp;' : ''}}
                                	<a>{{ $item->attribute->name }}</a>
                                	@endforeach
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                @endif
            </div>
        </div>
    </div>
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="pswp__bg"></div>
        <div class="pswp__scroll-wrap">
            <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
            </div>
            <div class="pswp__ui pswp__ui--hidden">
                <div class="pswp__top-bar">
                    <div class="pswp__counter"></div><button class="pswp__button pswp__button--close" aria-label="Đóng (Esc)"></button><button class="pswp__button pswp__button--share" aria-label="Chia sẻ"></button><button class="pswp__button pswp__button--fs" aria-label="Bật/tắt chế độ toàn màn hình"></button><button class="pswp__button pswp__button--zoom" aria-label="Phóng to/ thu nhỏ"></button>
                    <div class="pswp__preloader">
                        <div class="pswp__preloader__icn">
                            <div class="pswp__preloader__cut"></div>
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                    <div class="pswp__share-tooltip"></div>
                </div><button class="pswp__button pswp__button--arrow--left" aria-label="Ảnh trước (mũi tên trái)"></button><button class="pswp__button pswp__button--arrow--right" aria-label="Ảnh tiếp (mũi tên phải)"></button>
                <div class="pswp__caption">
                    <div class="pswp__caption__center"></div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
<script type="text/javascript">
    function addToCart(id,quantity)
    {
        var form = new FormData();
            form.append('_token','<?= csrf_token() ?>');
            form.append('id',id);
            form.append('quantity',quantity);
            $.ajax({
                url: "{{ route('frontend.cart.cart.add-cart') }}",
                data: form,
                processData: false,
                contentType: false,
                type: 'POST',
                beforeSend:function(){
                    $(".loading__container").addClass("active");
                },  
                success: function(data){
                    $(".loading__container").removeClass("active");
                    setTimeout(function(){ window.scrollTo(0, 0); }, 300);
                    $('#add-success').show();
                    $('#list-cart-to-product').html(data.data);
                    $('#total').text(data.total);
                    $('#cart-total').text(data.total);
                    $('#cart-count').text(data.count);
                }
            });
    }

   
    $(document).ready(function(){
        $(".button-add-to-cart").on("click", function(e) {
            e.preventDefault();
            var id = $(this).attr("data-product-id");
            var quantity = $('#quantity').val();
            var attr = $('#attribute');
            if (attr.length) {
                var stock = $(attr).find("option:selected").attr("data-stock");
                if(stock){
                    addToCart(id, quantity);
                }else{
                    $('.noti__error').show()
                }
            }else{
                addToCart(id, quantity);
            }
        });

    });
</script>
@endsection