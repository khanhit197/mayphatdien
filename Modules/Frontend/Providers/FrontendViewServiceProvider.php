<?php

namespace Modules\Frontend\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class FrontendViewServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function boot()
    {
        View::composer(
            [
                'frontend::frontend.layouts.footer',
            ], 
            'Modules\Frontend\Http\ViewComposers\ConfigComposer'
        );
        View::composer(
            [
                'frontend::frontend.layouts.header',
            ], 
            'Modules\Frontend\Http\ViewComposers\HeaderComposer'
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
