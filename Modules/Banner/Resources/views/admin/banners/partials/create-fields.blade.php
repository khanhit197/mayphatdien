
<div class="box-body">

	<div class='form-group{{ $errors->has('title') ? ' has-error' : '' }}'>
		{!! Form::label('title', trans('banner::banners.form.title')) !!}
		{!! Form::text('title', old('title'), ['class' => 'form-control', 'placeholder' => trans('banner::banners.form.title')]) !!}
		{!! $errors->first('Title', '<span class="help-block">:message</span>') !!}
	</div>

	<div class='form-group{{ $errors->has('description') ? ' has-error' : '' }}'>
		{!! Form::label('description', trans('banner::banners.form.description')) !!}
		{!! Form::text('description', old('description'), ['class' => 'form-control', 'placeholder' => trans('banner::banners.form.description')]) !!}
		{!! $errors->first('Description', '<span class="help-block">:message</span>') !!}
	</div>

	<div class='form-group{{ $errors->has('link') ? ' has-error' : '' }}'>
		{!! Form::label('link', trans('banner::banners.form.link')) !!}
		{!! Form::text('link', old('link'), ['class' => 'form-control', 'placeholder' => trans('banner::banners.form.link')]) !!}
		{!! $errors->first('Link', '<span class="help-block">:message</span>') !!}
	</div>
</div>