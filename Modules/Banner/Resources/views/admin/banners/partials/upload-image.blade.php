<div class="form-group">
    <label class="control-label"> Hình ảnh: </label>
    <div>
        <div class="images-container">
            <div id="show-file-ckfinder-input-1">
                @if(@$info['data']['image_vi'])
                <div class="image-container">
                    <div class="image" style="background-image:url(/{{$info['data']['image_vi']}} )"></div>
                </div>
                @endif
            </div>
            <input id="ckfinder-input-1" name="image" type="hidden" value="{{ @$info['data']['image'] }}">
            <a href="javascript:void(0)" class="add-image" id="ckfinder-popup-1">
                <div class="image-container new">
                    <div class="image">
                        <i class="fa fa-plus"></i>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>