<?php

namespace Modules\Banner\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface BannerRepository extends BaseRepository
{
	 /**
     * Create or update the settings
     * @param $settings
     * @return mixed
     */
    public function create($banner);


    /**
     * Find a setting by its name
     * @param $settingName
     * @return mixed
     */
    public function findByTitle($bannerName);


    /**
     * Find the given setting name for the given module
     * @param  string $settingName
     * @return mixed
     */
    public function get($bannerTitle);

    /**
     * Return the translatable module settings
     * @param $module
     * @return array
     */
    public function translatableModuleSettings($module);

}
