<?php

namespace Modules\Banner\Repositories\Eloquent;

use Modules\Banner\Repositories\BannerRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentBannerRepository extends EloquentBaseRepository implements BannerRepository
{
	/**
     * update the banner
     * @param $banner
     * @return mixed
     */
    public function create($banner){
    	return $this->model->create($banner);
    }

    /**
     * Find a setting by its name
     * @param $settingName
     * @return mixed
     */
    public function findByTitle($bannerName){

    }


    /**
     * Find the given setting name for the given module
     * @param  string $settingName
     * @return mixed
     */
    public function get($bannerTitle){

    }

    /**
     * Return the translatable module settings
     * @param $module
     * @return array
     */
    public function translatableModuleSettings($module){

    }
}
