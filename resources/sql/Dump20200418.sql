CREATE DATABASE  IF NOT EXISTS `mayphatdien_db` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `mayphatdien_db`;
-- MySQL dump 10.13  Distrib 8.0.19, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: mayphatdien_db
-- ------------------------------------------------------
-- Server version	8.0.19-0ubuntu0.19.10.3

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activations`
--

DROP TABLE IF EXISTS `activations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `activations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int unsigned NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activations`
--

LOCK TABLES `activations` WRITE;
/*!40000 ALTER TABLE `activations` DISABLE KEYS */;
INSERT INTO `activations` VALUES (1,1,'LqJHUxvbzMHIX6OSuS6egJVqXV5uvBJh',1,'2020-04-18 02:26:43','2020-04-18 02:26:43','2020-04-18 02:26:43');
/*!40000 ALTER TABLE `activations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard__widgets`
--

DROP TABLE IF EXISTS `dashboard__widgets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dashboard__widgets` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int unsigned NOT NULL,
  `widgets` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dashboard__widgets_user_id_foreign` (`user_id`),
  CONSTRAINT `dashboard__widgets_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard__widgets`
--

LOCK TABLES `dashboard__widgets` WRITE;
/*!40000 ALTER TABLE `dashboard__widgets` DISABLE KEYS */;
/*!40000 ALTER TABLE `dashboard__widgets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media__file_translations`
--

DROP TABLE IF EXISTS `media__file_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `media__file_translations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `file_id` int unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alt_attribute` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `media__file_translations_file_id_locale_unique` (`file_id`,`locale`),
  KEY `media__file_translations_locale_index` (`locale`),
  CONSTRAINT `media__file_translations_file_id_foreign` FOREIGN KEY (`file_id`) REFERENCES `media__files` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media__file_translations`
--

LOCK TABLES `media__file_translations` WRITE;
/*!40000 ALTER TABLE `media__file_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `media__file_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media__files`
--

DROP TABLE IF EXISTS `media__files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `media__files` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `is_folder` tinyint(1) NOT NULL DEFAULT '0',
  `filename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extension` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mimetype` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filesize` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `folder_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media__files`
--

LOCK TABLES `media__files` WRITE;
/*!40000 ALTER TABLE `media__files` DISABLE KEYS */;
INSERT INTO `media__files` VALUES (1,1,'images','/assets/media/images',NULL,NULL,NULL,'0','2020-04-18 07:08:30','2020-04-18 07:08:30'),(2,1,'banner','/assets/media/images/banner',NULL,NULL,NULL,'1','2020-04-18 07:08:45','2020-04-18 07:08:45'),(3,0,'banner-1.jpg','/assets/media/images/banner/banner-1.jpg','jpg','image/jpeg','490741','2','2020-04-18 07:08:54','2020-04-18 07:08:54'),(4,0,'banner-3.jpg','/assets/media/images/banner/banner-3.jpg','jpg','image/jpeg','481140','2','2020-04-18 07:08:54','2020-04-18 07:08:54'),(5,0,'banner-2.jpg','/assets/media/images/banner/banner-2.jpg','jpg','image/jpeg','557382','2','2020-04-18 07:08:55','2020-04-18 07:08:55'),(6,1,'tmp','/assets/media/images/tmp',NULL,NULL,NULL,'1','2020-04-18 07:32:09','2020-04-18 07:32:09'),(7,0,'sua-may-phat-dien-2018-3.jpg','/assets/media/images/tmp/sua-may-phat-dien-2018-3.jpg','jpg','image/jpeg','71376','6','2020-04-18 07:32:15','2020-04-18 07:32:15'),(9,0,'ezgif-7-dc8dff0fecfd.jpg','/assets/media/images/tmp/ezgif-7-dc8dff0fecfd.jpg','jpg','image/jpeg','67756','6','2020-04-18 07:39:37','2020-04-18 07:39:37'),(10,0,'3-untitled-8.jpg','/assets/media/3-untitled-8.jpg','jpg','image/jpeg','101770','0','2020-04-18 07:49:06','2020-04-18 07:49:06'),(11,0,'may-phat-dien-viet-nhat-100kva.jpg','/assets/media/images/tmp/may-phat-dien-viet-nhat-100kva.jpg','jpg','image/jpeg','54167','6','2020-04-18 10:14:45','2020-04-18 10:14:45'),(12,0,'may-phat-dien-cong-nghiep-viet-nhat-82.jpg','/assets/media/may-phat-dien-cong-nghiep-viet-nhat-82.jpg','jpg','image/jpeg','79466','0','2020-04-18 10:18:13','2020-04-18 10:18:13'),(13,0,'may-phat-dien-cong-nghiep-viet-nhat-82.jpg','/assets/media/images/tmp/may-phat-dien-cong-nghiep-viet-nhat-82.jpg','jpg','image/jpeg','79466','6','2020-04-18 10:18:32','2020-04-18 10:18:32');
/*!40000 ALTER TABLE `media__files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media__imageables`
--

DROP TABLE IF EXISTS `media__imageables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `media__imageables` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `file_id` int NOT NULL,
  `imageable_id` int NOT NULL,
  `imageable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media__imageables`
--

LOCK TABLES `media__imageables` WRITE;
/*!40000 ALTER TABLE `media__imageables` DISABLE KEYS */;
/*!40000 ALTER TABLE `media__imageables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu__menu_translations`
--

DROP TABLE IF EXISTS `menu__menu_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `menu__menu_translations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint NOT NULL DEFAULT '0',
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menu__menu_translations_menu_id_locale_unique` (`menu_id`,`locale`),
  KEY `menu__menu_translations_locale_index` (`locale`),
  CONSTRAINT `menu__menu_translations_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menu__menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu__menu_translations`
--

LOCK TABLES `menu__menu_translations` WRITE;
/*!40000 ALTER TABLE `menu__menu_translations` DISABLE KEYS */;
INSERT INTO `menu__menu_translations` VALUES (1,1,'en',1,'Menu Chính','2020-04-18 02:29:20','2020-04-18 02:34:44');
/*!40000 ALTER TABLE `menu__menu_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu__menuitem_translations`
--

DROP TABLE IF EXISTS `menu__menuitem_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `menu__menuitem_translations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `menuitem_id` int unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint NOT NULL DEFAULT '0',
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uri` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menu__menuitem_translations_menuitem_id_locale_unique` (`menuitem_id`,`locale`),
  KEY `menu__menuitem_translations_locale_index` (`locale`),
  CONSTRAINT `menu__menuitem_translations_menuitem_id_foreign` FOREIGN KEY (`menuitem_id`) REFERENCES `menu__menuitems` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu__menuitem_translations`
--

LOCK TABLES `menu__menuitem_translations` WRITE;
/*!40000 ALTER TABLE `menu__menuitem_translations` DISABLE KEYS */;
INSERT INTO `menu__menuitem_translations` VALUES (1,1,'en',0,'root',NULL,NULL,'2020-04-18 02:29:20','2020-04-18 02:29:20'),(2,2,'en',1,'Trang chủ','/',NULL,'2020-04-18 02:30:19','2020-04-18 02:30:19'),(3,3,'en',1,'Sản phẩm',NULL,NULL,'2020-04-18 02:30:45','2020-04-18 02:30:45'),(4,4,'en',1,'Máy phát điện Cummins','/may-phat-dien-cummins',NULL,'2020-04-18 02:31:24','2020-04-18 02:45:57'),(5,5,'en',1,'Máy phát điện Denyo','/may-phat-dien-denyo',NULL,'2020-04-18 02:32:32','2020-04-18 02:46:15');
/*!40000 ALTER TABLE `menu__menuitem_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu__menuitems`
--

DROP TABLE IF EXISTS `menu__menuitems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `menu__menuitems` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int unsigned NOT NULL,
  `page_id` int unsigned DEFAULT NULL,
  `position` int unsigned NOT NULL DEFAULT '0',
  `target` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'page',
  `class` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '',
  `module_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int DEFAULT NULL,
  `lft` int DEFAULT NULL,
  `rgt` int DEFAULT NULL,
  `depth` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_root` tinyint(1) NOT NULL DEFAULT '0',
  `icon` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menu__menuitems_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu__menuitems_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menu__menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu__menuitems`
--

LOCK TABLES `menu__menuitems` WRITE;
/*!40000 ALTER TABLE `menu__menuitems` DISABLE KEYS */;
INSERT INTO `menu__menuitems` VALUES (1,1,NULL,0,NULL,'page','',NULL,NULL,NULL,NULL,NULL,'2020-04-18 02:29:20','2020-04-18 02:29:20',1,NULL),(2,1,NULL,0,'_self','external',NULL,NULL,1,NULL,NULL,NULL,'2020-04-18 02:30:19','2020-04-18 02:30:19',0,NULL),(3,1,NULL,1,'_self','page',NULL,NULL,1,NULL,NULL,NULL,'2020-04-18 02:30:45','2020-04-18 02:31:26',0,NULL),(4,1,NULL,1,'_self','external',NULL,NULL,3,NULL,NULL,NULL,'2020-04-18 02:31:24','2020-04-18 07:03:49',0,NULL),(5,1,NULL,0,'_self','external',NULL,NULL,3,NULL,NULL,NULL,'2020-04-18 02:32:32','2020-04-18 07:03:49',0,NULL);
/*!40000 ALTER TABLE `menu__menuitems` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu__menus`
--

DROP TABLE IF EXISTS `menu__menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `menu__menus` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `primary` tinyint NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu__menus`
--

LOCK TABLES `menu__menus` WRITE;
/*!40000 ALTER TABLE `menu__menus` DISABLE KEYS */;
INSERT INTO `menu__menus` VALUES (1,'generate',0,'2020-04-18 02:29:20','2020-04-18 02:34:21');
/*!40000 ALTER TABLE `menu__menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_07_02_230147_migration_cartalyst_sentinel',1),(2,'2016_06_24_193447_create_user_tokens_table',1),(3,'2014_10_14_200250_create_settings_table',2),(4,'2014_10_15_191204_create_setting_translations_table',2),(5,'2015_06_18_170048_make_settings_value_text_field',2),(6,'2015_10_22_130947_make_settings_name_unique',2),(7,'2017_09_17_164631_make_setting_value_nullable',2),(8,'2014_11_03_160015_create_menus_table',3),(9,'2014_11_03_160138_create_menu-translations_table',3),(10,'2014_11_03_160753_create_menuitems_table',3),(11,'2014_11_03_160804_create_menuitem_translation_table',3),(12,'2014_12_17_185301_add_root_column_to_menus_table',3),(13,'2015_09_05_100142_add_icon_column_to_menuitems_table',3),(14,'2016_01_26_102307_update_icon_column_on_menuitems_table',3),(15,'2016_08_01_142345_add_link_type_colymn_to_menuitems_table',3),(16,'2016_08_01_143130_add_class_column_to_menuitems_table',3),(17,'2017_09_18_192639_make_title_field_nullable_menu_table',3),(18,'2014_10_26_162751_create_files_table',4),(19,'2014_10_26_162811_create_file_translations_table',4),(20,'2015_02_27_105241_create_image_links_table',4),(21,'2015_12_19_143643_add_sortable',4),(22,'2017_09_20_144631_add_folders_columns_on_files_table',4),(23,'2014_11_30_191858_create_pages_tables',5),(24,'2017_10_13_103344_make_status_field_nullable_on_page_translations_table',5),(25,'2018_05_23_145242_edit_body_column_nullable',5),(26,'2015_04_02_184200_create_widgets_table',6),(27,'2013_04_09_062329_create_revisions_table',7),(28,'2015_11_20_184604486385_create_translation_translations_table',7),(29,'2015_11_20_184604743083_create_translation_translation_translations_table',7),(30,'2015_12_01_094031_update_translation_translations_table_with_index',7),(31,'2016_07_12_181155032011_create_tag_tags_table',8),(32,'2016_07_12_181155289444_create_tag_tag_translations_table',8);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page__page_translations`
--

DROP TABLE IF EXISTS `page__page_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `page__page_translations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '1',
  `body` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `meta_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `og_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `og_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `og_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `og_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `page__page_translations_page_id_locale_unique` (`page_id`,`locale`),
  KEY `page__page_translations_locale_index` (`locale`),
  CONSTRAINT `page__page_translations_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `page__pages` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page__page_translations`
--

LOCK TABLES `page__page_translations` WRITE;
/*!40000 ALTER TABLE `page__page_translations` DISABLE KEYS */;
INSERT INTO `page__page_translations` VALUES (1,1,'en','Home page','home','1','<p><strong>You made it!</strong></p>\n<p>You&#39;ve installed AsgardCMS and are ready to proceed to the <a href=\"/en/backend\">administration area</a>.</p>\n<h2>What&#39;s next ?</h2>\n<p>Learn how you can develop modules for AsgardCMS by reading our <a href=\"https://github.com/AsgardCms/Documentation\">documentation</a>.</p>\n','Home page',NULL,NULL,NULL,NULL,NULL,'2020-04-18 02:26:53','2020-04-18 02:26:53');
/*!40000 ALTER TABLE `page__page_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page__pages`
--

DROP TABLE IF EXISTS `page__pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `page__pages` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `is_home` tinyint(1) NOT NULL DEFAULT '0',
  `template` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page__pages`
--

LOCK TABLES `page__pages` WRITE;
/*!40000 ALTER TABLE `page__pages` DISABLE KEYS */;
INSERT INTO `page__pages` VALUES (1,1,'home','2020-04-18 02:26:53','2020-04-18 02:26:53');
/*!40000 ALTER TABLE `page__pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persistences`
--

DROP TABLE IF EXISTS `persistences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `persistences` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int unsigned NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `persistences_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persistences`
--

LOCK TABLES `persistences` WRITE;
/*!40000 ALTER TABLE `persistences` DISABLE KEYS */;
INSERT INTO `persistences` VALUES (1,1,'PnDVqBWMBqQi5giG7lOl4FZj9S9FKrLD','2020-04-18 02:27:17','2020-04-18 02:27:17'),(2,1,'4Cb9sgdABfWhtbWwO4kkYwCjmxHaEAaH','2020-04-18 02:27:18','2020-04-18 02:27:18'),(3,1,'GIr1pgM1OOlkGVyUfDZ5ixLWxsdmq47S','2020-04-18 02:27:48','2020-04-18 02:27:48'),(4,1,'x1ev2nz4c47XtDCm1vbiA1fbogREPxnv','2020-04-18 02:27:48','2020-04-18 02:27:48'),(5,1,'HLRXvSvY9Tic1XXbsQhswC7zuSdKw702','2020-04-18 02:28:21','2020-04-18 02:28:21'),(6,1,'beEoRO9L0EudsUMH1HqzRETlHxX7XEPA','2020-04-18 02:31:26','2020-04-18 02:31:26'),(7,1,'MDkJ0Fe0JNKc9EvWcYdjc9sk0VuQwB7r','2020-04-18 02:31:30','2020-04-18 02:31:30'),(8,1,'C80DBTyIewKSFjnqpZNTr6c3Jvp9KCet','2020-04-18 02:31:32','2020-04-18 02:31:32'),(9,1,'ovDSam3lmK4oyZvnZ26cy4LP86blJaaw','2020-04-18 02:31:34','2020-04-18 02:31:34'),(10,1,'rDVFx5nl0x8kn8HKbEAQ6tnkMuuRzKsn','2020-04-18 02:46:02','2020-04-18 02:46:02'),(11,1,'nf4g8U9AYmYBWrenx4fezWPl1tEuLKr7','2020-04-18 06:55:09','2020-04-18 06:55:09'),(12,1,'8RZXr5l6BoR7mzE2q0e3ppeWhSQcRz6P','2020-04-18 07:03:49','2020-04-18 07:03:49'),(13,1,'DUlP2EowwmEadndmfKXP5QxWuywuNYDo','2020-04-18 07:08:21','2020-04-18 07:08:21'),(14,1,'DEYE7mdQqyDpCD3AG2tXg2NPWbefvA7z','2020-04-18 07:08:30','2020-04-18 07:08:30'),(15,1,'N0TSMiDxpyZcxub15adjAFpY7xqoYpRl','2020-04-18 07:08:30','2020-04-18 07:08:30'),(16,1,'mMs2LbYRteHQgIYbURjrTH9yrsjLkJWT','2020-04-18 07:08:39','2020-04-18 07:08:39'),(17,1,'aQNzMxFwE1XZMvxRqAnnC69MHE97hsqt','2020-04-18 07:08:40','2020-04-18 07:08:40'),(18,1,'DhWEWCGErANMaEoyeAeKPlMYeyxib8jT','2020-04-18 07:08:45','2020-04-18 07:08:45'),(19,1,'TwpeLH7bRWOEOgWTIaxuPEyINvM64y5L','2020-04-18 07:08:46','2020-04-18 07:08:46'),(20,1,'tJCvIGvqzvkxfBiQe744v9Ypp0vSkL44','2020-04-18 07:08:47','2020-04-18 07:08:47'),(21,1,'nxCCsJQt3xnlNp1VROmmqGqRtZHAPClT','2020-04-18 07:08:47','2020-04-18 07:08:47'),(22,1,'CR5DxNYzKI6zIporQe3nVaxuncOm82AN','2020-04-18 07:08:54','2020-04-18 07:08:54'),(23,1,'0kRUU5kMwIHWAUKlVOMl88b4o2OhNCKT','2020-04-18 07:08:54','2020-04-18 07:08:54'),(24,1,'w3CBtFl3tAXcNQlTtt3JF3G55UsV38U5','2020-04-18 07:08:55','2020-04-18 07:08:55'),(25,1,'qMY5gw54AORPl1kqW3JTECHh4wVmeQLk','2020-04-18 07:08:55','2020-04-18 07:08:55'),(26,1,'zSiRWKSIu3X09SnFMAShIYutyKle1EKY','2020-04-18 07:08:55','2020-04-18 07:08:55'),(27,1,'OQquh41czcXgcoDOIsJsZf34OHlOguqo','2020-04-18 07:08:55','2020-04-18 07:08:55'),(28,1,'5LSZXRwJW1gyknAwVxY7xS68ywut4kjb','2020-04-18 07:09:02','2020-04-18 07:09:02'),(29,1,'RjdQPJCjT82Fo50BFlxyHZ8hk0lY8F1n','2020-04-18 07:31:57','2020-04-18 07:31:57'),(30,1,'z7UBPcalKTz1qwH0nIpn8egCelAFcCUT','2020-04-18 07:32:00','2020-04-18 07:32:00'),(31,1,'Dya5MwWIbsf7fivddJHW8b6spuheNbNE','2020-04-18 07:32:00','2020-04-18 07:32:00'),(32,1,'R48rTtNjxgdXfE4Wtqr2hwgYzeeI9S6t','2020-04-18 07:32:09','2020-04-18 07:32:09'),(33,1,'6153d0Y9l3NTiUo9V7TJsDI6ZMLpvL6x','2020-04-18 07:32:09','2020-04-18 07:32:09'),(34,1,'iT8yelsiqvTcthDTpCDrPy9tg1HnzNR1','2020-04-18 07:32:11','2020-04-18 07:32:11'),(35,1,'R4ZaKCpY8eChzmId33eRpaQ0YPFJYTca','2020-04-18 07:32:11','2020-04-18 07:32:11'),(36,1,'W5Had2a2TX1QQ9Ie625fZirOOXyOk9wU','2020-04-18 07:32:15','2020-04-18 07:32:15'),(37,1,'jN8P1hr6YWDZKmylOGvI2qLxAlHQ3yYe','2020-04-18 07:32:15','2020-04-18 07:32:15'),(38,1,'KgYjXOX8mzdKTIBFakdqgNlcdtUUymEP','2020-04-18 07:32:24','2020-04-18 07:32:24'),(39,1,'qstl9xHDpcNnuAkLfmBNGC7R4Ny6CqTg','2020-04-18 07:35:45','2020-04-18 07:35:45'),(40,1,'XwXnNydFKowN0g8I9b3a65WMDK59rzMF','2020-04-18 07:35:45','2020-04-18 07:35:45'),(41,1,'AX4gE4B3LDFAB8Gem51YuPze67A9fKKB','2020-04-18 07:36:14','2020-04-18 07:36:14'),(42,1,'sCaS16SZvzM24xJtmzkCwLIWuTwLxLZv','2020-04-18 07:39:01','2020-04-18 07:39:01'),(43,1,'b6C5orIqvhiladreFXedz7zirLe3Pw8l','2020-04-18 07:39:01','2020-04-18 07:39:01'),(44,1,'pVnfhRmxqI2tupjvIZJqSaHKNbVZaukP','2020-04-18 07:39:01','2020-04-18 07:39:01'),(45,1,'o0lWggTVYi9kGW03gD4t6dLp6Qkgt7PH','2020-04-18 07:39:08','2020-04-18 07:39:08'),(46,1,'JYojmMTS4y8CIdMVesjrDvaDQruf8l9A','2020-04-18 07:39:13','2020-04-18 07:39:13'),(47,1,'xWZt6DG5NoPYxXxZsqrzro0sqDbMq1Fn','2020-04-18 07:39:13','2020-04-18 07:39:13'),(48,1,'kZC1w0g714lFwTgLUilHojYjLo2wTIMs','2020-04-18 07:39:20','2020-04-18 07:39:20'),(49,1,'ULqpSYZSRw1yNse0MRvMsNdkCw1Zj8QF','2020-04-18 07:39:23','2020-04-18 07:39:23'),(50,1,'yGGUCYhAtNEuTUCZK3YCwPXjS2nedw7T','2020-04-18 07:39:28','2020-04-18 07:39:28'),(51,1,'eham9cV40GPnRUaUTHHY9KqkfCIZULdn','2020-04-18 07:39:31','2020-04-18 07:39:31'),(52,1,'LYwYkeXDY25C3GRIBn34dKbGuLdwlkql','2020-04-18 07:39:32','2020-04-18 07:39:32'),(53,1,'JmhXBYBNY5p9zSW4SAmYVZXsCwIvAJZE','2020-04-18 07:39:32','2020-04-18 07:39:32'),(54,1,'gO6CEj1eYzU1vEOLXlvzmghLHQBCGJqO','2020-04-18 07:39:33','2020-04-18 07:39:33'),(55,1,'ccKpOD2oCTyCuUJ3YRDtlhSajoqlBs6D','2020-04-18 07:39:33','2020-04-18 07:39:33'),(56,1,'UXOccGSjgg8ccJFjp2RclOUft5yeYejS','2020-04-18 07:39:37','2020-04-18 07:39:37'),(57,1,'FLe2oSvqXULmdtDLwm4OosgSdQjR1Veo','2020-04-18 07:39:37','2020-04-18 07:39:37'),(58,1,'YIbwNGgRSHr7y0BtXGCFpCsyiPySJJJ4','2020-04-18 07:39:37','2020-04-18 07:39:37'),(59,1,'cNSWKOPRthjfNA6d8AbKAK57Tk2siQTC','2020-04-18 07:39:37','2020-04-18 07:39:37'),(60,1,'Kde6u77AxbqpHEIBwJsJjDaS8Y18cCW4','2020-04-18 07:39:37','2020-04-18 07:39:37'),(61,1,'Ewo3nbddjqQi5gznwbXfRvsfwH7GxyYb','2020-04-18 07:39:42','2020-04-18 07:39:42'),(62,1,'uxlQdBpb1lnQn7hOZ7CKrTOtNMJZOS1L','2020-04-18 07:49:00','2020-04-18 07:49:00'),(63,1,'1JdVZhFFnSO1VNDtZdPUr0qGN631wKr5','2020-04-18 07:49:00','2020-04-18 07:49:00'),(64,1,'QsdJsF3a7XB8e7duK7snRUGJcF0iWML5','2020-04-18 07:49:06','2020-04-18 07:49:06'),(65,1,'TK3KzODhOPbcdV4z2n5KujOvQNNhWLBt','2020-04-18 07:49:06','2020-04-18 07:49:06'),(66,1,'he1jSAMpI9P4pca4HpRX5Y2JdcQIqHSD','2020-04-18 07:49:07','2020-04-18 07:49:07'),(67,1,'HS75IHsLVB0tGGK6DnOj9UKdtj1gK5mB','2020-04-18 07:49:07','2020-04-18 07:49:07'),(68,1,'qOlSx2xUTUSRjAcY4V1vdlWUWLnTA1U7','2020-04-18 07:49:07','2020-04-18 07:49:07'),(69,1,'hapNIJRTUwE6vl7USeSFRSxbElfFk8Rf','2020-04-18 07:49:07','2020-04-18 07:49:07'),(70,1,'FOKy7Cz0C2ddAUPefa8MaIyiwnqjBmfT','2020-04-18 07:49:11','2020-04-18 07:49:11'),(71,1,'nUk5aBjQ7FlQI87ViflkzVYtbCYZCLYP','2020-04-18 10:14:35','2020-04-18 10:14:35'),(72,1,'KLh55reKu4dT4WCQwENrY5Qey9C3useO','2020-04-18 10:14:41','2020-04-18 10:14:41'),(73,1,'yXHeOZbGqiD7moQB3keAuFJAST5IYjDi','2020-04-18 10:14:41','2020-04-18 10:14:41'),(74,1,'qkMJHUVEhC7ZnaDR4krSv5Ue1yesxROc','2020-04-18 10:14:42','2020-04-18 10:14:42'),(75,1,'ltMkW0AeEbDEGbuaRaioWBnLUT7Vr3tR','2020-04-18 10:14:42','2020-04-18 10:14:42'),(76,1,'tI3hgwr9r15eXzTXzAhFvUGt68LUOlvc','2020-04-18 10:14:44','2020-04-18 10:14:44'),(77,1,'CgnNdEpG9cfjdgOqIwwKO0le3r7rbP17','2020-04-18 10:14:45','2020-04-18 10:14:45'),(78,1,'Xf3YkAIjM5fdZr2jR7zRUM6i8Ae9T5Wx','2020-04-18 10:14:47','2020-04-18 10:14:47'),(79,1,'ThLBMKO42ssoMcz58VvkoSBlY1bvjibv','2020-04-18 10:16:25','2020-04-18 10:16:25'),(80,1,'fVY97uS8XJtpfxq6KxfWgEIGz0r387Xo','2020-04-18 10:18:06','2020-04-18 10:18:06'),(81,1,'gn13mq9F911SZVwlslf31ikiLFKnyDqV','2020-04-18 10:18:06','2020-04-18 10:18:06'),(82,1,'mhCMsnw0oy3sqonnQ5X4aklaiB2b4Zaf','2020-04-18 10:18:13','2020-04-18 10:18:13'),(83,1,'IcT8BDbyNibeJxyF9G8u7Aeg9V3nPcY1','2020-04-18 10:18:13','2020-04-18 10:18:13'),(84,1,'IJL7GvdLQlBrRdqhHwgwm4tpK0eVaaTk','2020-04-18 10:18:18','2020-04-18 10:18:18'),(85,1,'kw1MR8tuDJ2HPdtcv8oYX1gisCKsf41c','2020-04-18 10:18:19','2020-04-18 10:18:19'),(86,1,'YBw60kndCavmiQmUTzEq2pfH5EznmK9V','2020-04-18 10:18:22','2020-04-18 10:18:22'),(87,1,'6FMFyNGCaj1rIFjPXLkj6PyetAxOTeOR','2020-04-18 10:18:22','2020-04-18 10:18:22'),(88,1,'1nWNwYKDdqXKVeUXxK0pG0dCLfyU6qpS','2020-04-18 10:18:26','2020-04-18 10:18:26'),(89,1,'qTuOyzdMVsgJLngwQUX6dIpN9g0NWTnr','2020-04-18 10:18:26','2020-04-18 10:18:26'),(90,1,'JR2s4zd6UIsM581J4YLm9SNJReEXX6gS','2020-04-18 10:18:27','2020-04-18 10:18:27'),(91,1,'OujRCV76nRgeUL7v7g27qKYly87zxouc','2020-04-18 10:18:27','2020-04-18 10:18:27'),(92,1,'d9f1e8Uh0iz6Eq6vCYkw6ki4IF2yOseP','2020-04-18 10:18:32','2020-04-18 10:18:32'),(93,1,'FlAiXTLhhuz0zqFZHJ57d2I06LtNF0EP','2020-04-18 10:18:32','2020-04-18 10:18:32'),(94,1,'dXPVvWSqvfWgP2BdNP40bn5eJ30RBCxM','2020-04-18 10:18:37','2020-04-18 10:18:37'),(95,1,'SBMF9YJO9SqzC5ePYZpxywkYeZ2IUruH','2020-04-18 10:18:53','2020-04-18 10:18:53');
/*!40000 ALTER TABLE `persistences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reminders`
--

DROP TABLE IF EXISTS `reminders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reminders` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int unsigned NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reminders`
--

LOCK TABLES `reminders` WRITE;
/*!40000 ALTER TABLE `reminders` DISABLE KEYS */;
/*!40000 ALTER TABLE `reminders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `revisions`
--

DROP TABLE IF EXISTS `revisions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `revisions` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `revisionable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revisionable_id` int NOT NULL,
  `user_id` int DEFAULT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_value` text COLLATE utf8mb4_unicode_ci,
  `new_value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `revisions_revisionable_id_revisionable_type_index` (`revisionable_id`,`revisionable_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `revisions`
--

LOCK TABLES `revisions` WRITE;
/*!40000 ALTER TABLE `revisions` DISABLE KEYS */;
/*!40000 ALTER TABLE `revisions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_users`
--

DROP TABLE IF EXISTS `role_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role_users` (
  `user_id` int unsigned NOT NULL,
  `role_id` int unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_users`
--

LOCK TABLES `role_users` WRITE;
/*!40000 ALTER TABLE `role_users` DISABLE KEYS */;
INSERT INTO `role_users` VALUES (1,1,'2020-04-18 02:26:43','2020-04-18 02:26:43');
/*!40000 ALTER TABLE `role_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin','Admin','{\"core.sidebar.group\":true,\"dashboard.index\":true,\"dashboard.update\":true,\"dashboard.reset\":true,\"workshop.sidebar.group\":true,\"workshop.modules.index\":true,\"workshop.modules.show\":true,\"workshop.modules.update\":true,\"workshop.modules.disable\":true,\"workshop.modules.enable\":true,\"workshop.modules.publish\":true,\"workshop.themes.index\":true,\"workshop.themes.show\":true,\"workshop.themes.publish\":true,\"user.roles.index\":true,\"user.roles.create\":true,\"user.roles.edit\":true,\"user.roles.destroy\":true,\"user.users.index\":true,\"user.users.create\":true,\"user.users.edit\":true,\"user.users.destroy\":true,\"account.api-keys.index\":true,\"account.api-keys.create\":true,\"account.api-keys.destroy\":true,\"menu.menus.index\":true,\"menu.menus.create\":true,\"menu.menus.edit\":true,\"menu.menus.destroy\":true,\"menu.menuitems.index\":true,\"menu.menuitems.create\":true,\"menu.menuitems.edit\":true,\"menu.menuitems.destroy\":true,\"media.medias.index\":true,\"media.medias.create\":true,\"media.medias.edit\":true,\"media.medias.destroy\":true,\"media.folders.index\":true,\"media.folders.create\":true,\"media.folders.edit\":true,\"media.folders.destroy\":true,\"setting.settings.index\":true,\"setting.settings.edit\":true,\"page.pages.index\":true,\"page.pages.create\":true,\"page.pages.edit\":true,\"page.pages.destroy\":true,\"translation.translations.index\":true,\"translation.translations.edit\":true,\"translation.translations.export\":true,\"translation.translations.import\":true,\"tag.tags.index\":true,\"tag.tags.create\":true,\"tag.tags.edit\":true,\"tag.tags.destroy\":true}','2020-04-18 02:26:25','2020-04-18 02:26:25'),(2,'user','User',NULL,'2020-04-18 02:26:25','2020-04-18 02:26:25');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `setting__setting_translations`
--

DROP TABLE IF EXISTS `setting__setting_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `setting__setting_translations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `setting_id` int unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `setting__setting_translations_setting_id_locale_unique` (`setting_id`,`locale`),
  KEY `setting__setting_translations_locale_index` (`locale`),
  CONSTRAINT `setting__setting_translations_setting_id_foreign` FOREIGN KEY (`setting_id`) REFERENCES `setting__settings` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `setting__setting_translations`
--

LOCK TABLES `setting__setting_translations` WRITE;
/*!40000 ALTER TABLE `setting__setting_translations` DISABLE KEYS */;
INSERT INTO `setting__setting_translations` VALUES (1,3,'en','Dashboard',NULL),(2,4,'en',NULL,NULL);
/*!40000 ALTER TABLE `setting__setting_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `setting__settings`
--

DROP TABLE IF EXISTS `setting__settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `setting__settings` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `plainValue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `isTranslatable` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `setting__settings_name_unique` (`name`),
  KEY `setting__settings_name_index` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `setting__settings`
--

LOCK TABLES `setting__settings` WRITE;
/*!40000 ALTER TABLE `setting__settings` DISABLE KEYS */;
INSERT INTO `setting__settings` VALUES (1,'core::template','Flatly',0,'2020-04-18 02:26:53','2020-04-18 02:26:53'),(2,'core::locales','[\"en\"]',0,'2020-04-18 02:26:53','2020-04-18 02:26:53'),(3,'dashboard::welcome-title',NULL,1,'2020-04-18 07:02:52','2020-04-18 07:02:52'),(4,'dashboard::welcome-description',NULL,1,'2020-04-18 07:02:52','2020-04-18 07:02:52'),(5,'dashboard::welcome-enabled','1',0,'2020-04-18 07:02:52','2020-04-18 07:02:52');
/*!40000 ALTER TABLE `setting__settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag__tag_translations`
--

DROP TABLE IF EXISTS `tag__tag_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tag__tag_translations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag_id` int unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tag__tag_translations_tag_id_locale_unique` (`tag_id`,`locale`),
  KEY `tag__tag_translations_locale_index` (`locale`),
  CONSTRAINT `tag__tag_translations_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tag__tags` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag__tag_translations`
--

LOCK TABLES `tag__tag_translations` WRITE;
/*!40000 ALTER TABLE `tag__tag_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `tag__tag_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag__tagged`
--

DROP TABLE IF EXISTS `tag__tagged`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tag__tagged` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `taggable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `taggable_id` int unsigned NOT NULL,
  `tag_id` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tag__tagged_taggable_type_taggable_id_index` (`taggable_type`,`taggable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag__tagged`
--

LOCK TABLES `tag__tagged` WRITE;
/*!40000 ALTER TABLE `tag__tagged` DISABLE KEYS */;
/*!40000 ALTER TABLE `tag__tagged` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag__tags`
--

DROP TABLE IF EXISTS `tag__tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tag__tags` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `namespace` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag__tags`
--

LOCK TABLES `tag__tags` WRITE;
/*!40000 ALTER TABLE `tag__tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `tag__tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `throttle`
--

DROP TABLE IF EXISTS `throttle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `throttle` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int unsigned DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `throttle_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `throttle`
--

LOCK TABLES `throttle` WRITE;
/*!40000 ALTER TABLE `throttle` DISABLE KEYS */;
INSERT INTO `throttle` VALUES (1,NULL,'global',NULL,'2020-04-18 02:27:13','2020-04-18 02:27:13'),(2,NULL,'ip','127.0.0.1','2020-04-18 02:27:13','2020-04-18 02:27:13');
/*!40000 ALTER TABLE `throttle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translation__translation_translations`
--

DROP TABLE IF EXISTS `translation__translation_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `translation__translation_translations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `translation_id` int unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_trans_id_locale_unique` (`translation_id`,`locale`),
  KEY `translation__translation_translations_locale_index` (`locale`),
  CONSTRAINT `translation__translation_translations_translation_id_foreign` FOREIGN KEY (`translation_id`) REFERENCES `translation__translations` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translation__translation_translations`
--

LOCK TABLES `translation__translation_translations` WRITE;
/*!40000 ALTER TABLE `translation__translation_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `translation__translation_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translation__translations`
--

DROP TABLE IF EXISTS `translation__translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `translation__translations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `translation__translations_key_index` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translation__translations`
--

LOCK TABLES `translation__translations` WRITE;
/*!40000 ALTER TABLE `translation__translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `translation__translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_tokens`
--

DROP TABLE IF EXISTS `user_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_tokens` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int unsigned NOT NULL,
  `access_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_tokens_access_token_unique` (`access_token`),
  KEY `user_tokens_user_id_foreign` (`user_id`),
  CONSTRAINT `user_tokens_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_tokens`
--

LOCK TABLES `user_tokens` WRITE;
/*!40000 ALTER TABLE `user_tokens` DISABLE KEYS */;
INSERT INTO `user_tokens` VALUES (1,1,'9fad4a7d-8228-4ff6-a380-dd3b67b485b3','2020-04-18 02:26:44','2020-04-18 02:26:44');
/*!40000 ALTER TABLE `user_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `last_login` timestamp NULL DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'khanhit197@gmail.com','$2y$10$JnvDBYxmC5k2nEyFeyMkh.fdsHaYvD6iDaZBhFBIcQiVmntK0C83u',NULL,'2020-04-18 10:18:53','nguyen','khanh','2020-04-18 02:26:43','2020-04-18 10:18:53');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-18 17:26:55
